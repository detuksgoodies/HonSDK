#include "stdafx.h"
#include "Game.hpp"

namespace HonSDK
{
	static Game::Game()
	{
		ATTACH_DOMAIN();
		
		ATTACH_EVENT
		(
			GameWndProc,
			1, Native::OnWndProc, HWND, UINT, WPARAM, LPARAM
		);
		ATTACH_EVENT
		(
			GameUpdate,
			2, Native::OnMainLoop
		);	
	}

	void Game::DomainUnloadEventHandler(Object^, EventArgs^)
	{
		DETACH_EVENT
		(
			GameWndProc,
			1, Native::OnWndProc, HWND, UINT, WPARAM, LPARAM
		);
		DETACH_EVENT
		(
			GameUpdate,
			2, Native::OnMainLoop
		);
	}

	bool Game::OnGameWndProcNative(HWND HWnd, UINT message, WPARAM WParam, LPARAM LParam)
	{
		bool process = true;

		START_TRACE
			auto args = gcnew WndEventArgs(HWnd, message, WParam, LParam);
		for each (auto eventHandle in GameWndProcHandlers->ToArray())
		{
			START_TRACE
				eventHandle(args);
			END_TRACE
		}

		if (!args->Process)
			process = false;
		END_TRACE

		return process;
	}

	void Game::OnGameUpdateNative()
	{
		START_TRACE
			for each (auto eventHandle in GameUpdateHandlers->ToArray())
			{
				START_TRACE
					eventHandle(EventArgs::Empty);
				END_TRACE
			}
		END_TRACE
	}

}

