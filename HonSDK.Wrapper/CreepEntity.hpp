#pragma once
#include "../../HonSDK/HonSDK/CreepEntity.h"

#include "UnitEntity.hpp"

using namespace System;
using namespace SharpDX;

namespace HonSDK
{
	public ref class CreepEntity : public UnitEntity {
	internal:
		Native::CreepEntity* GetPtr()
		{
			return reinterpret_cast<Native::CreepEntity*>(UnitEntity::GetPtr());
		}

	protected:
		int _networkId;
		Native::CreepEntity* self;

	public:
		CreepEntity(int networkId, Native::GameEntity* unit) : UnitEntity(networkId, unit) {}
		CreepEntity::CreepEntity() {};
		//static UnitEntity::UnitEntity() {};
	};

}