#pragma once
#include "../../HonSDK/HonSDK/Drawing.h"
#include "../../HonSDK/HonSDK/GameInternal.h"
#include "../../HonSDK/HonSDK/EventHandler.h"

#include "Marcos.hpp"

using namespace SharpDX;

namespace HonSDK
{
	MAKE_EVENT_GLOBAL(DrawingEndScene, EventArgs^ args);

	public ref class Drawing
	{
		static SharpDX::Direct3D9::Device^ m_device;
		static void* oldDX;
	internal:
		MAKE_EVENT_INTERNAL(DrawingEndScene, ());
	public:
		MAKE_EVENT_PUBLIC(OnEndScene, DrawingEndScene);

		static Drawing();
		static void DomainUnloadEventHandler(System::Object^, System::EventArgs^);

		static void DrawFontText(float x, float y, System::Drawing::Color color, String^ test, int size);
		static void DrawLine(float x, float y, float x2, float y2, float thickness, System::Drawing::Color color);

		static property SharpDX::Direct3D9::Device^ Direct3DDevice
		{
			SharpDX::Direct3D9::Device^ get();
		}

	};
}