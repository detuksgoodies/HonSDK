#pragma once
#include "../../HonSDK/HonSDK/NeutralEntity.h"

#include "UnitEntity.hpp"

using namespace System;
using namespace SharpDX;

namespace HonSDK
{
	public ref class NeutralEntity : public UnitEntity {
	internal:
		Native::NeutralEntity* GetPtr()
		{
			return reinterpret_cast<Native::NeutralEntity*>(UnitEntity::GetPtr());
		}

	protected:
		int _networkId;
		Native::NeutralEntity* self;

	public:
		NeutralEntity(int networkId, Native::GameEntity* unit) : UnitEntity(networkId, unit) {}
		NeutralEntity::NeutralEntity() {};
		//static UnitEntity::UnitEntity() {};
	};

}