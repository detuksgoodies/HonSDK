#pragma once
#include "../../HonSDK/HonSDK/GameEntity.h"
#include "../../HonSDK/HonSDK/EventHandler.h"

#include "Marcos.hpp"
#include "GameEntityCreateEventArgs.hpp"

using namespace System;
using namespace SharpDX;

namespace HonSDK
{
	MAKE_EVENT_GLOBAL(GameEntityCreate, GameEntity^ sender, EventArgs^ args);
	MAKE_EVENT_GLOBAL(GameEntityDelete, GameEntity^ sender, EventArgs^ args);

	public ref class GameEntity {
	internal:
		MAKE_EVENT_INTERNAL(GameEntityCreate, (Native::GameEntity* entity));
		MAKE_EVENT_INTERNAL(GameEntityDelete, (int entityNetId));
		Native::GameEntity* GetPtr();
	protected:
		int _networkId;
		Native::GameEntity* self;

	public:
		//static event GameEntityCreate^ OnCreate { 
		//	void add(GameEntityCreate^ handler) { GameEntityCreateHandlers->Add(handler); } 
		//	void remove(GameEntityCreate^ handler) { GameEntityCreateHandlers->Remove(handler); } 
		//}

		MAKE_EVENT_PUBLIC(OnCreate, GameEntityCreate );
		MAKE_EVENT_PUBLIC(OnDelete, GameEntityDelete );

		GameEntity(int networkId, Native::GameEntity *);
		GameEntity() {};
		static GameEntity();
		static void DomainUnloadEventHandler(System::Object ^, System::EventArgs ^);

		property String^ Name
		{
			String^ get()
			{
				return gcnew String(*this->GetPtr()->GetTypeName());
			}
		}

		property IntPtr MemoryAddress
		{
			IntPtr get()
			{
				return static_cast<IntPtr>(this->GetPtr());
			}
		}

		property int NetworkId
		{
			int get()
			{
				return static_cast<int>(this->_networkId);
			}
		}

	};

}