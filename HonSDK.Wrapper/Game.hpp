#pragma once
#include "../../HonSDK/HonSDK/Game.h"
#include "../../HonSDK/HonSDK/GameInternal.h"
#include "../../HonSDK/HonSDK/EventHandler.h"

#include "Marcos.hpp"
#include "GameWndEventArgs.hpp"

using namespace System;
using namespace System::Text;
using namespace System::Collections::Generic;
using namespace System::Threading;

namespace HonSDK
{

	MAKE_EVENT_GLOBAL(GameWndProc, WndEventArgs^ args);
	MAKE_EVENT_GLOBAL(GameUpdate, System::EventArgs^ args);

	public ref class Game
	{

	internal:
		MAKE_EVENT_INTERNAL_PROCESS(GameWndProc, (HWND, UINT, WPARAM, LPARAM));
		MAKE_EVENT_INTERNAL(GameUpdate, ());
	public:
		MAKE_EVENT_PUBLIC(OnWndProc, GameWndProc);
		MAKE_EVENT_PUBLIC(OnUpdate, GameUpdate);

		static Game::Game();

		static void DomainUnloadEventHandler(Object^, EventArgs^);

		static property int MyHeroNetworkId
		{
			int get()
			{
				auto internalGame = Native::GameInternal::GetInstance();
				if (internalGame != nullptr)
				{
					auto player = internalGame->GetCurrentPlayer();
					if (player != nullptr)
						return *player->GetHeroEntityNetId();
				}
				return -1;
			}
		}

		static property bool IsGamePlaying
		{
			bool get()
			{
				auto internalGame = Native::GameInternal::GetInstance();
				if(internalGame != nullptr)
				{
					return internalGame->IsPlaying();
				}
				return false;
			}
		}

		static property int GameState
		{
			int get()
			{
				auto internalGame = Native::GameInternal::GetInstance();
				if (internalGame != nullptr)
				{
					return *internalGame->GetInfo()->GetGameState();
				}
				return -1;
			}
		}

		static property SharpDX::Vector3 CursorPos
		{
			SharpDX::Vector3 get()
			{
				auto internalGame = Native::GameInternal::GetInstance();
				if (internalGame != nullptr)
				{
					auto cursorConteiner = internalGame->GetGameCursorContainer();
					if (cursorConteiner != nullptr)
					{
						auto vec = *cursorConteiner->GetCursorPos();
						return SharpDX::Vector3(vec.GetX(), vec.GetY(), vec.GetZ());
					}
				}
				return SharpDX::Vector3(0,0,0);
			}
		}

	};
}