#pragma once
#include "../../HonSDK/HonSDK/UnitEntity.h"

#include "UnitEntity.hpp"

using namespace System;
using namespace SharpDX;

namespace HonSDK
{
	public ref class Projectile : public UnitEntity {
	internal:
		Native::UnitEntity* GetPtr()
		{
			return reinterpret_cast<Native::UnitEntity*>(UnitEntity::GetPtr());
		}

	protected:
		int _networkId;
		Native::UnitEntity* self;

	public:
		Projectile(int networkId, Native::GameEntity* unit) : UnitEntity(networkId, unit) {}
		Projectile::Projectile() {};
		//static UnitEntity::UnitEntity() {};
	};

}