#include "stdafx.h"
#include "GameEntity.hpp"
#include "ObjectManager.hpp"
#include <msclr\marshal_cppstd.h>

using namespace System;
using namespace SharpDX;
using namespace System::Runtime::InteropServices;
using namespace System::Collections::Generic;

namespace HonSDK
{

	Native::GameEntity * GameEntity::GetPtr()
	{
		if (this->self != nullptr)
		{
			return this->self;
		}
		throw gcnew Exception("Got pointer to null object!");
		return nullptr;
	}

	GameEntity::GameEntity(int networkId, Native::GameEntity * unit)
	{
		this->_networkId = networkId;
		this->self = unit;
	}

	static GameEntity::GameEntity()
	{
		ATTACH_DOMAIN();
		ATTACH_EVENT
		(
			GameEntityCreate,
			8, Native::OnGameEntityCreate, Native::GameEntity*
		);
		ATTACH_EVENT
		(
			GameEntityDelete,
			9, Native::OnGameEntityDelete, int
		);
	}

	void GameEntity::DomainUnloadEventHandler(System::Object^, System::EventArgs^)
	{
		DETACH_EVENT
		(
			GameEntityCreate,
			8, Native::OnGameEntityCreate, Native::GameEntity*
		);
		DETACH_EVENT
		(
			GameEntityDelete,
			9, Native::OnGameEntityDelete, int
		);
	}

	void GameEntity::OnGameEntityCreateNative(Native::GameEntity* obj)
	{
		START_TRACE
			if (obj != nullptr)
			{
				if (GameEntityCreateHandlers->Count > 0)
				{
					GameEntity^ sender = ObjectManager::CreateObjectFromPointer(obj);

					if (sender == nullptr)
					{
						return;
					}

					for each (auto eventHandle in GameEntityCreateHandlers->ToArray())
					{
						START_TRACE
							eventHandle(sender, EventArgs::Empty);
						END_TRACE
					}
				}
			}
		END_TRACE
	}

	void GameEntity::OnGameEntityDeleteNative(int entityNetId)
	{
		START_TRACE
			GameEntity^ sender = ObjectManager::GetFromNetworkId(entityNetId);
			if (sender == nullptr)
				return;

			for each (auto eventHandle in GameEntityDeleteHandlers->ToArray())
			{
				START_TRACE
					eventHandle(sender, EventArgs::Empty);
				END_TRACE
			}
		END_TRACE
	}

}