#pragma once
#include "../../HonSDK/HonSDK/HeroEntity.h"

#include "UnitEntity.hpp"

using namespace System;
using namespace SharpDX;

namespace HonSDK
{
	public ref class HeroEntity : public UnitEntity {
	internal:
		Native::HeroEntity* GetPtr()
		{
			return reinterpret_cast<Native::HeroEntity*>(UnitEntity::GetPtr());
		}

	protected:
		int _networkId;
		Native::HeroEntity* self;

	public:
		HeroEntity(int networkId, Native::GameEntity* unit) : UnitEntity(networkId, unit) {}
		HeroEntity::HeroEntity() {};
		//static UnitEntity::UnitEntity() {};
	};

}