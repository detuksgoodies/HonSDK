#pragma once
#include "stdafx.h"

#include "GameEntity.hpp"

namespace HonSDK
{
	ref class GameEntity;

	public ref class GameEntityCreateEventArgs : public System::EventArgs
	{
	private:
		GameEntity^ obj;
	public:
		delegate void GameEntityCreateEvent(GameEntity^ sender, System::EventArgs^ args);

		GameEntityCreateEventArgs(GameEntity^ sender)
		{
			this->obj = sender;
		}

		property HonSDK::GameEntity^ GameObject
		{
			HonSDK::GameEntity^ get()
			{
				return this->obj;
			}
		}
	};
}