#include "Stdafx.h"
#include "Drawing.hpp"

using namespace System::Runtime::InteropServices;
using namespace SharpDX::Direct3D9;
using namespace System::Collections::Generic;

namespace HonSDK
{
	static Drawing::Drawing()
	{
		ATTACH_DOMAIN();

		ATTACH_EVENT(DrawingEndScene, 3, Native::OnEndScene);

		//ATTACH_EVENT( DrawingHealthbars, 80, Native::OnDrawingHealthBars, Native::UnitInfoComponent*, Native::AttackableUnit*);
	}
	void Drawing::DomainUnloadEventHandler(System::Object^, System::EventArgs^)
	{
		DETACH_EVENT(DrawingEndScene, 3, Native::OnEndScene);
		//DETACH_EVENT( DrawingHealthbars, 80, Native::OnDrawingHealthBars, Native::UnitInfoComponent*, Native::AttackableUnit* );
	}

	void Drawing::DrawFontText(float x, float y, System::Drawing::Color color, System::String^ text, int size)
	{
		auto hText = Marshal::StringToHGlobalAnsi(text);
		Native::Drawing::DrawFontText(x, y, color.ToArgb(), (char*)hText.ToPointer(), size);
		Marshal::FreeHGlobal(hText);
	}

	void Drawing::DrawLine(float x, float y, float x2, float y2, float thickness, System::Drawing::Color color)
	{
		Native::Drawing::DrawLine(x, y, x2, y2, thickness, color.ToArgb());
	}

	SharpDX::Direct3D9::Device^ Drawing::Direct3DDevice::get()
	{
		IDirect3DDevice9* d3d9Device = Native::DirectX9::GetInstance()->GetDevice();
		if (d3d9Device != nullptr
			&& d3d9Device != Drawing::oldDX)
		{
			Drawing::oldDX = d3d9Device;
			Drawing::m_device = gcnew SharpDX::Direct3D9::Device((IntPtr)d3d9Device);
		}

		return Drawing::m_device;
	}

	void Drawing::OnDrawingEndSceneNative()
	{
		START_TRACE
			for each (auto eventHandle in DrawingEndSceneHandlers->ToArray())
			{
				START_TRACE
					eventHandle(EventArgs::Empty);
				END_TRACE
			}
		END_TRACE
	}

}
