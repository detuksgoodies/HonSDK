#pragma once
#include "../../HonSDK/HonSDK/GameEntity.h"
#include "../../HonSDK/HonSDK/ObjectManager.h"

#include "GameEntity.hpp"
#include "HeroEntity.hpp"
#include "CreepEntity.hpp"
#include "Marcos.hpp"

#pragma comment(lib, "HonSDK.lib")

using namespace System;
using namespace System::Linq;
using namespace System::Collections::Generic;

namespace HonSDK
{
	public ref class ObjectManager
	{
	internal:
		static void RefreshCache();
		static bool cacheCreated = false;
		value class EntityPredicate
		{
			IntPtr address;
		public:
			EntityPredicate(IntPtr address)
			{
				this->address = address;
			}
			bool IsMatch(GameEntity ^entity)
			{
				return entity->MemoryAddress == address;
			}
		};

	public:
		static GameEntity^ CreateObjectFromPointer(Native::GameEntity* obj);
		static GameEntity ^ GetFromNetworkId(int networkId);
		static void OnEntityCreate(GameEntity ^sender, EventArgs ^args);
		static void OnEntityDelete(GameEntity ^sender, EventArgs ^args);

		static List<GameEntity^>^ GetAll();
		static List<CreepEntity^>^ GetCreeps();
		static List<HeroEntity^>^ GetHeroes();

		static ObjectManager();

		static property HeroEntity^ Player
		{
			HeroEntity^ get()
			{
				return m_cachedPlayer;
			}
		}

		generic <typename ObjectType>
		where ObjectType : GameEntity, gcnew()
		static ObjectType GetUnitByNetworkId(int networkId)
		{
			Native::GameEntity* nativeUnit = Native::ObjectManager::GetGameEntityByNetworkId(networkId);
			if (nativeUnit != nullptr)
			{
				return (ObjectType)CreateObjectFromPointer(nativeUnit);
			}

			return ObjectType();
		}


	private:
		static HeroEntity^ m_cachedPlayer;
		static Dictionary<int, GameEntity^>^ cachedEntities = gcnew Dictionary<int, GameEntity^>(2000);
		static HashSet<int>^ cachedPlayerNetIds = gcnew HashSet<int>();
		static HashSet<int>^ cachedCreepNetIds = gcnew HashSet<int>();

		static void CacheEntity(GameEntity ^sender);
		static void DecacheEntity(int netId);

		generic <typename ObjectType>
			where ObjectType : GameEntity, gcnew()
				static IEnumerable<ObjectType>^ Get()
			{
				RefreshCache();
				if (GameEntity::typeid == ObjectType::typeid)
				{
					return Enumerable::Cast<ObjectType>(cachedEntities);
				}

				return Enumerable::OfType<ObjectType>(cachedEntities);
			}
	};
}