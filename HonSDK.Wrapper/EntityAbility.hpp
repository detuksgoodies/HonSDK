#pragma once
#include "../../HonSDK/HonSDK/EntityAbility.h"

using namespace System;
using namespace SharpDX;

namespace HonSDK
{
	public ref class EntityAbility 
	{
	internal:
		EntityAbility(Native::EntityAbility* ability, int slot)
		{
			this->ability = ability;
			this->slot = slot;
		}
	private:
		int slot;
		Native::EntityAbility* ability;
	public:

		property String^ AbilityName
		{
			String^ get()
			{
				return gcnew String(*ability->GetAbilityName());
			}
		}

		property float ManaCost
		{
			float get()
			{
				return ability->GetManaCost();
			}
		}

		property int AbilityLevel
		{
			int get()
			{
				return *ability->GetAbilityLevel();
			}
		}

		bool IsReady()
		{
			return ability->IsReady();
		}
		
		int GetActualRemainingCooldownTime()
		{
			return ability->GetActualRemainingCooldownTime();
		}
		
		int GetRemainingCooldownTime(bool smth)
		{
			return ability->GetRemainingCooldownTime(smth);
		}
		
		int GetCooldownEndTime()
		{
			return ability->GetCooldownEndTime();
		}
		
		int GetDynamicRange(bool smth)
		{
			return ability->GetDynamicRange(smth);
		}

	};
}