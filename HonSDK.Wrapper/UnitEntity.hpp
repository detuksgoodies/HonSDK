#pragma once
#include "../../HonSDK/HonSDK/UnitEntity.h"
#include "../../HonSDK/HonSDK/GameInternal.h"
#include "../../HonSDK/HonSDK/Action.h"

#include "GameEntity.hpp"
#include "EntityAbility.hpp"

using namespace System;
using namespace SharpDX;

namespace HonSDK
{
	public ref class UnitEntity : public GameEntity {
	internal:
		Native::UnitEntity* GetPtr()
		{
			return reinterpret_cast<Native::UnitEntity*>(GameEntity::GetPtr());
		}

	protected:
		int _networkId;
		Native::UnitEntity* self;

	public:
		UnitEntity(int networkId, Native::GameEntity* unit) : GameEntity(networkId, unit) {}
		UnitEntity::UnitEntity() {};

		property Vector3 Position
		{
			Vector3 get()
			{
				auto pos = *this->GetPtr()->GetPos();
				return Vector3(pos.GetX(), pos.GetY(), pos.GetZ());
			}
		}

		property Vector2 Direction
		{
			Vector2 get()
			{
				auto pos = *this->GetPtr()->GetDirection();
				return Vector2(-pos.GetY(), pos.GetX());
			}
		}

		property float DirectionDegrees
		{
			float get()
			{
				return *this->GetPtr()->GetDirectionDegrees();
			}
		}

		property float Health
		{
			float get()
			{
				return *this->GetPtr()->GetHealth();
			}
		}

		property float MaxHealth
		{
			float get()
			{
				return *this->GetPtr()->GetMaxHealth();
			}
		}

		property float Mana
		{
			float get()
			{
				return *this->GetPtr()->GetMana();
			}
		}

		property float MaxMana
		{
			float get()
			{
				return *this->GetPtr()->GetMaxMana();
			}
		}

		property float BoundingRadius
		{
			float get()
			{
				return this->GetPtr()->GetBoundsRadius();
			}
		}

		property float AttackRange
		{
			float get()
			{
				return this->GetPtr()->GetAttackRange();
			}
		}

		property float BaseAttackSpeed
		{
			float get()
			{
				return this->GetPtr()->GetBaseAttackSpeed();
			}
		}

		property float Armor
		{
			float get()
			{
				return this->GetPtr()->GetArmor();
			}
		}

		property float MagicArmor
		{
			float get()
			{
				return this->GetPtr()->GetMagicArmor();
			}
		}

		property double PhysicalResistance
		{
			double get()
			{
				return Native::GameInternal::GetInstance()->GetMechanics()->GetArmorDamageAdjustment(this->GetPtr()->GetArmorPercentage(), this->GetPtr()->GetArmor());
			}
		}

		property double MagicalResistance
		{
			double get()
			{
				return Native::GameInternal::GetInstance()->GetMechanics()->GetArmorDamageAdjustment(this->GetPtr()->GetMagicArmorPercentage(), this->GetPtr()->GetMagicArmor());
			}
		}

		property float AttackSpeed
		{
			float get()
			{
				return this->GetPtr()->GetAttackSpeed();
			}
		}

		property double AttackDamangeMin
		{
			double get()
			{
				return this->GetPtr()->GetAttackDamangeBaseMin() + this->GetPtr()->GetAttackDamangeAdded();
			}
		}

		property double AttackDamangeMax
		{
			double get()
			{
				return this->GetPtr()->GetAttackDamangeBaseMax() + this->GetPtr()->GetAttackDamangeAdded();
			}
		}

		property double AttackDamangeBaseMin
		{
			double get()
			{
				return this->GetPtr()->GetAttackDamangeBaseMin();
			}
		}

		property double AttackDamangeBaseMax
		{
			double get()
			{
				return this->GetPtr()->GetAttackDamangeBaseMax();
			}
		}

		property double AdjustedAttackActionTime
		{
			double get()
			{
				return this->GetPtr()->GetAdjustedAttackActionTime();
			}
		}

		property double AdjustedAttackDuration
		{
			double get()
			{
				return this->GetPtr()->GetAdjustedAttackDuration();
			}
		}

		property double AdjustedAttackCooldown
		{
			double get()
			{
				return this->GetPtr()->GetAdjustedAttackCooldown();
			}
		}

		property bool Visible
		{
			bool get()
			{
				auto pos = *this->GetPtr()->GetPos();
				return pos.GetX() > 1 || pos.GetY() > 1;
			}
		}

		bool IsEnemy(UnitEntity^ entity)
		{
			return this->GetPtr()->IsEnemy(entity->GetPtr());
		}

		EntityAbility^ getAbility(int slot)
		{
			return gcnew EntityAbility(this->GetPtr()->GetAbility(slot), slot);
		}

		void Cast(int slot)
		{
			Native::Action::CastSpell(this->GetPtr(), slot);
		}

		void Cast(int slot, float x, float y)
		{
			Native::Action::CastSpell(this->GetPtr(), slot, x, y);
		}

		void Cast(int slot, Vector3 vec)
		{
			Cast(slot, vec.X, vec.Y);
		}

		void Cast(int slot, Vector2 vec)
		{
			Cast(slot, vec.X, vec.Y);
		}

		void Cast(int slot, GameEntity^ unit)
		{
			Native::Action::CastSpell(this->GetPtr(), slot, unit->GetPtr());
		}

		void Attack(GameEntity^ unit)
		{
			Native::Action::Attack(unit->GetPtr());
		}

		void Attack(GameEntity^ unit, int flag)
		{
			Native::Action::Attack(unit->GetPtr(), flag);
		}

		void Hold()
		{
			//TODO: select previous selected units
			Native::Action::SelectEntity(this->GetPtr(), 1);
			Native::Action::Hold();
		}

		void Move(float x, float y, int flag)
		{
			//TODO: select previous selected units
			Native::Action::SelectEntity(this->GetPtr(), 1);
			Native::Action::Move(x, y, flag);
		}

		void Move(float x, float y)
		{
			Move(x, y, 2);
		}

		void Move(Vector2 vec)
		{
			Move(vec.X, vec.Y, 2);
		}

		void Move(Vector3 vec)
		{
			Move(vec.X, vec.Y, 2);
		}

		void Move(Vector2 vec, int flag)
		{
			Move(vec.X, vec.Y, flag);
		}

		void Move(Vector3 vec, int flag)
		{
			Move(vec.X, vec.Y, flag);
		}

	};

}