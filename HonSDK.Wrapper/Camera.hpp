#pragma once
#include "../../HonSDK/HonSDK/Camera.h"
#include "../../HonSDK/HonSDK/GameInternal.h"

#include "Marcos.hpp"

using namespace System;
using namespace SharpDX;

namespace HonSDK
{

	public ref class Camera
	{
	public:
		static Vector2 WorldToScreen(Vector3 worldLocation)
		{
			auto camera = Native::GameInternal::GetInstance()->GetGameCamera();
			if (camera == nullptr)
				return Vector2(-1000,-1000);
			Native::Vector3f wLoc(worldLocation.X, worldLocation.Y, worldLocation.Z);
			Native::Vector2f wOut;
			camera->WorldToScreen(&wLoc, &wOut);

			return Vector2(wOut.GetX(), wOut.GetY());
		}

	};
}