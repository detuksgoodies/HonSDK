#include "stdafx.h"
#include "ObjectManager.hpp"

#include "GameEntity.hpp"
#include "UnitEntity.hpp"
#include "CreepEntity.hpp"
#include "Projectile.hpp"
#include "NeutralEntity.hpp"
#include "Game.hpp"

#define OBJECT_FROM_POINTER(OBJECT_TYPE, POINTER)\
	if(HonSDK::ObjectManager::cachedEntities->ContainsKey(*POINTER->GetNetworkId()))\
		return HonSDK::ObjectManager::cachedEntities[*POINTER->GetNetworkId()];\
	return gcnew OBJECT_TYPE(*POINTER->GetNetworkId(), POINTER);

namespace HonSDK
{

	static ObjectManager::ObjectManager()
	{
		Console::WriteLine("Init ObjectManager!");
		ObjectManager::cacheCreated = false;
		GameEntity::OnCreate += gcnew GameEntityCreate(&ObjectManager::OnEntityCreate);
		GameEntity::OnDelete += gcnew GameEntityDelete(&ObjectManager::OnEntityDelete);
		//TODO: add update if injected through many games
		RefreshCache();
	}
	
	GameEntity ^ ObjectManager::GetFromNetworkId(int networkId)
	{
		if (networkId < 0 || !ObjectManager::cachedEntities->ContainsKey(networkId))
			return nullptr;
		return ObjectManager::cachedEntities[networkId];
	}

	GameEntity ^ ObjectManager::CreateObjectFromPointer(Native::GameEntity * obj)
	{
		if (obj == nullptr || (obj->GetNetworkId() == nullptr))
		{
			return nullptr;
		}

		if (obj->IsHero())
		{
			OBJECT_FROM_POINTER(HeroEntity, obj);
		}

		if(obj ->IsCreep())
		{
			OBJECT_FROM_POINTER(CreepEntity, obj);
		}

		if (obj->IsProjectile())
		{
			OBJECT_FROM_POINTER(Projectile, obj);
		}

		if (obj->IsNeutral())
		{
			OBJECT_FROM_POINTER(NeutralEntity, obj);
		}

		return gcnew GameEntity(*obj->GetNetworkId(), obj);
	}

	List<GameEntity^>^ ObjectManager::GetAll()
	{
		return gcnew List<GameEntity^>(ObjectManager::cachedEntities->Values);
	}

	List<HeroEntity^>^ ObjectManager::GetHeroes()
	{
		auto heroes = gcnew List<HeroEntity^>(ObjectManager::cachedPlayerNetIds->Count);
		for each(int netId in ObjectManager::cachedPlayerNetIds)
		{
			heroes->Add((HeroEntity^)ObjectManager::cachedEntities[netId]);
		}
		return heroes;
	}

	List<CreepEntity^>^ ObjectManager::GetCreeps()
	{
		auto creeps = gcnew List<CreepEntity^>(ObjectManager::cachedCreepNetIds->Count);
		for each(int netId in ObjectManager::cachedCreepNetIds)
		{
			creeps->Add((CreepEntity^)ObjectManager::cachedEntities[netId]);
		}
		return creeps;
	}


	void ObjectManager::OnEntityCreate(GameEntity ^sender, EventArgs ^args)
	{
		ObjectManager::CacheEntity(sender);
		if(ObjectManager::m_cachedPlayer == nullptr)
		{
			cacheCreated = false;
			RefreshCache();
		}
	}

	void ObjectManager::OnEntityDelete(GameEntity ^sender, EventArgs ^args)
	{
		if (sender != nullptr)
		{
			ObjectManager::DecacheEntity(sender->NetworkId);
		}
	}

	void ObjectManager::DecacheEntity(int netId)
	{
			ObjectManager::cachedPlayerNetIds->Remove(netId);
			ObjectManager::cachedCreepNetIds->Remove(netId);
			int myHeroNetworkId = Game::MyHeroNetworkId;
			if (myHeroNetworkId == netId)
				ObjectManager::m_cachedPlayer = nullptr;

		//Console::WriteLine("Add new entity with netId: " + sender->NetworkId);
		ObjectManager::cachedEntities->Remove(netId);

	}

	void ObjectManager::CacheEntity(GameEntity ^sender)
	{
		if (sender == nullptr)
			return;

		if (sender->GetType() == CreepEntity::typeid)
			ObjectManager::cachedCreepNetIds->Add(sender->NetworkId);
		if (sender->GetType() == HeroEntity::typeid)
		{
			ObjectManager::cachedPlayerNetIds->Add(sender->NetworkId);
			int myHeroNetworkId = Game::MyHeroNetworkId;
			if (myHeroNetworkId == sender->NetworkId)
				ObjectManager::m_cachedPlayer = (HeroEntity^)sender;
		}

		//Console::WriteLine("Add new entity with netId: " + sender->NetworkId);
		ObjectManager::cachedEntities->Remove(sender->NetworkId);
		ObjectManager::cachedEntities->Add(sender->NetworkId, sender);
		
	}

	void ObjectManager::RefreshCache()
	{
		if (cacheCreated /*|| !Game::IsGamePlaying*/)
			return;
		Console::WriteLine("Refreshing cache!");
		cacheCreated = true;

		std::vector<Native::GameEntity*> entities;
		Native::ObjectManager::GetEntities(&entities);

		for (Native::GameEntity* entity : entities)
		{
			if (entity == nullptr)
				continue;
			auto managedObject = ObjectManager::CreateObjectFromPointer(entity);
			ObjectManager::CacheEntity(managedObject);
		}

	}
}