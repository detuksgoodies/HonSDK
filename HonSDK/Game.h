#pragma once
#include "Macros.h"
#include "Detour.hpp"
#include "GameEntity.h"
#include <memory>

namespace HonSDK
{
	namespace Native
	{
		class Buffer;

		class
			DLLEXPORT Game
		{
		private:
			static std::unique_ptr<std::vector<GameEntity*>> SpawnedEntities;
			static int LastNetworkId;
			Game();
		public:
			static Game* GetInstance();
			bool ApplyHooks() const;
			bool SendGameData(Buffer* buffer, bool reliable);
		};
	}
}
