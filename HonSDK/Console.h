#pragma once
#include <Windows.h>

#include "Macros.h"

namespace HonSDK
{
	namespace Native
	{
		class DLLEXPORT Console
		{
		public:
			static void Create();
			static void PrintLn();
			static void PrintLn(const char* format, ...);
			static void Print(const char* format, ...);
			static void Show();
			static void Hide();

			static HWND GetHandle();
		};
	}
}
