#pragma once

#include "DirectX9.h"
#include "Macros.h"

namespace HonSDK
{
	namespace Native
	{
		class DLLEXPORT Drawing
		{


		public:
			static ID3DXLine* g_pLine;
			static ID3DXFont* g_pFont;

			static void DrawFontText(float x, float y, D3DCOLOR color, char* text, int size = 25);
			static void DrawLine(float x, float y, float x2, float y2, float thickness, D3DCOLOR color);
			static SIZE GetTextEntent(char* text, int size);
		};
	}
}