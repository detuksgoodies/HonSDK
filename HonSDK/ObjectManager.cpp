#include "stdafx.h"
#include "ObjectManager.h"
#include "Core.h"
#include "Offsets.h"
#include "GameInternal.h"

#define ENTITY_WRAPPER_SIZE 0x690

namespace HonSDK
{
	namespace Native
	{
		GameEntity** ObjectManager::GetEntityArray()
		{
			return nullptr;
		}

		ClientEntity* ObjectManager::GetClientEntityArray()
		{
			return *reinterpret_cast<ClientEntity**>((DWORD)Core::hCGame + static_cast<__int32>(Offsets::Global::ClientEntityArray));
		}

		ClientEntity* ObjectManager::GetClientEntity(int index)
		{
			__try
			{
			return reinterpret_cast<ClientEntity*>((DWORD)GetClientEntityArray() + ENTITY_WRAPPER_SIZE * index);
			}
			__except (EXCEPTION_EXECUTE_HANDLER)
			{
				return nullptr;
			}
		}

		GameEntity * ObjectManager::GetGameEntityByIndex(int index)
		{
			auto clientEntity = GetClientEntity(index);
			if (clientEntity == nullptr)
				return nullptr;
			return clientEntity->GetEntity();
		}

		GameEntity * Native::ObjectManager::GetGameEntityByNetworkId(int netowrkId)
		{
			for (int i = 0; i < ObjectManager::GetEntityWrapperArraySize(); i++)
			{
				auto clientEntity = ObjectManager::GetClientEntity(i);
				if (clientEntity == nullptr)
					continue;
				auto gameEntity = clientEntity->GetEntity();
				if (gameEntity == nullptr)
					continue;
				if (*gameEntity->GetNetworkId() == netowrkId)
					return gameEntity;
			}

			return nullptr;
		}

		HeroEntity * ObjectManager::GetMyHero()
		{
			auto player = GameInternal::GetInstance()->GetCurrentPlayer();
			if (player == nullptr)
				return nullptr;
			int heroId = *player->GetHeroEntityNetId();
			std::vector<GameEntity*> entities;
			ObjectManager::GetEntities(&entities);

			for (GameEntity* entity : entities)
			{
				if (*entity->GetNetworkId() == heroId)
					return (HeroEntity*)entity;
			}
			return nullptr;
		}

		int ObjectManager::GetEntityWrapperArraySize()
		{
			return *reinterpret_cast<int*>((DWORD)Core::hCGame + static_cast<__int32>(Offsets::Global::ClientEntityArraySize));
		}

		void ObjectManager::GetHeroEntities(std::vector<UnitEntity*>* entities)
		{
			for (int i = 0; i < ObjectManager::GetEntityWrapperArraySize(); i++)
			{
				auto clientEntity = ObjectManager::GetClientEntity(i);
				if (clientEntity == nullptr)
					continue;
				auto gameEntity = clientEntity->GetEntity();
				if (gameEntity == nullptr)
					continue;
				if (!gameEntity->IsHero())
					continue;
				auto unit = (UnitEntity*)gameEntity;
				entities->push_back(unit);

			}
		}

		void Native::ObjectManager::GetEntities(std::vector<GameEntity*>* entities)
		{
			for (int i = 0; i < ObjectManager::GetEntityWrapperArraySize(); i++)
			{
				auto clientEntity = ObjectManager::GetClientEntity(i);
				if (clientEntity == nullptr)
					continue;
				auto gameEntity = clientEntity->GetEntity();
				if (gameEntity == nullptr)
					continue;
				entities->push_back(gameEntity);

			}
		}

	}
}
