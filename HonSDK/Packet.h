#pragma once
#include "Macros.h"

namespace HonSDK
{
	namespace Native
	{
		// This is only used by server :/
		struct Packet
		{
			int Flags;
			int Unknown4;
			int VirtualTable;
			int DataStartIndex;
			int MaxSize;
			int Size;
			int Unknown18;
			int Unknown1C;
			unsigned char Data[0x1000];

			//MAKE_GET(int, Flags, 0x0);
			//MAKE_GET_PTR(void, VirtualTable, 0x8);
			//MAKE_GET(int, DataStartIndex, 0xC);
			//MAKE_GET(int, MaxSize, 0x10);//0x3FF9
			//MAKE_GET(int, Size, 0x14);
			//MAKE_GET(unsigned char, FirstByte, 0x20);
			// 
			// ...... Alll info goes here
			//
			MAKE_GET(unsigned char, LastByteSmth, 0x401C);
			
		};
	}
}