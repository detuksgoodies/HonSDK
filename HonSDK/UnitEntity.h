#pragma once
#include <math.h>
#include "Macros.h"
#include "GameEntity.h"
#include "EntityAbility.h"
#include "Vector3f.h"
#include "Vector2f.h"

//#include "GameInternal.h"

namespace HonSDK
{
	namespace Native
	{

		class GameInternal;

		class DLLEXPORT UnitEntity : public GameEntity
		{
		public:
			MAKE_GET(Vector3f, Pos, 0xA4);
			//MAKE_GET(float, Y, 0xA8);
			//MAKE_GET(float, Z, 0xAC);

			MAKE_GET(float, DirectionDegrees, 0xD0);

			MAKE_GET(int, TargetIndex, 0x128); //Doesnt seem to work

			MAKE_GET(Vector2f, Direction, 0x1F4); //Doesnt seem to work

			MAKE_GET(float, Health, 0x2A8);
			MAKE_GET(float, MaxHealth, 0x2B0);

			MAKE_GET(float, Mana, 0x2AC);
			MAKE_GET(float, MaxMana, 0x2B4);

			MAKE_GET(EntityAbility*, Abilities, 0x340);

			MAKE_GET(int, OwnerPlayerId, 0x4A4);

			EntityAbility* GetAbility(int slot)
			{
				auto ability = (GetAbilities())[slot];
				return ability;
			}
			//PhysicalResistance
			IMPLVTFUNC(int, GetArmorPercentage, 0x37C);
			IMPLVTFUNC(int, GetMagicArmorPercentage, 0x384);

			IMPLVTFUNC(float, GetArmor, 0x38C);
			IMPLVTFUNC(float, GetMagicArmor, 0x390);

			IMPLVTFUNC(float, GetAttackRange, 0x3D0);
			IMPLVTFUNC(float, GetBaseAttackSpeed, 0x3D4);
			IMPLVTFUNC(float, GetAttackSpeed, 0x3E0);
			
			IMPLVTFUNC(float, GetBoundsRadius, 0x5F8);

			IMPLVTFUNC(double, GetAttackDamangeBaseMin, 0x64C);
			IMPLVTFUNC(double, GetAttackDamangeBaseMax, 0x650);

			IMPLVTFUNC(float, GetAttackDamangeAdded, 0x428);

			IMPLVTFUNC(float, GetAttackDamangeBaseMin1, 0x650);//??
			IMPLVTFUNC(float, GetAttackDamangeBaseMin2, 0x418);//??
			IMPLVTFUNC(float, GetAttackDamangeBaseMin4, 0x420);//??




			IMPLVTFUNC(bool, CanAttack, 0x6CC);

			IMPLVTFUNC(double, GetAdjustedAttackActionTime, 0x830);
			IMPLVTFUNC(double, GetAdjustedAttackDuration, 0x834);
			IMPLVTFUNC(double, GetAdjustedAttackCooldown, 0x838);

			//Doesnt work :(
			bool IsAttackReady()
			{
				return !IsAttackReadyStuff3() ||
					*GetAttackReadyStuff1() == -1 ||
					*GetAttackReadyStuff2() & 4;
			}

			MAKE_GET(int, AttackReadyStuff1, 0x52C);
			MAKE_GET(char, AttackReadyStuff2, 0x2A0);
			IMPLVTFUNC(int, IsAttackReadyStuff3, 0x468);

			bool IsEnemy(UnitEntity* entity)
			{
				return ((bool(__fastcall*)(UnitEntity*, int, UnitEntity*))Exports::GetInstance()->IsEnemy)(this, 0, entity);
			}

			int GetActivePath()
			{
				return ((bool(__fastcall*)(UnitEntity*, int))Exports::GetInstance()->GetActivePath)(this, 0);
			}

			//float GetArmorDamageAdjusted()
			//{
			//	float armor = GetArmor();
			//	if(armor < 0)
			//	{
			//		auto v5 = 1.0 -
			//	}
			//	return 
			//}

		};
		
	}
}
