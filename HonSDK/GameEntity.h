#pragma once
#include "vadefs.h"
#include "Macros.h"

namespace HonSDK
{
	namespace Native
	{
		class DLLEXPORT GameEntity
		{
		public:
			MAKE_GET(short, NetworkId, 0x10);
			MAKE_GET(wchar_t*, TypeName, 0x20);

			IMPLVTFUNC(bool, IsHero, 0xB4)

			bool IsCreep()
			{
				return !wcsncmp(*GetTypeName(), L"Creep_", 6);
			}

			bool IsNeutral()
			{
				return !wcsncmp(*GetTypeName(), L"Neutral_", 8);
			}

			bool IsPowerup()
			{
				return !wcsncmp(*GetTypeName(), L"Powerup_", 8);
			}

			bool IsCritter()
			{
				return !wcsncmp(*GetTypeName(), L"Critter_", 8);
			}

			bool IsAffector()
			{
				return !wcsncmp(*GetTypeName(), L"Affector_", 9);
			}

			bool IsGadget()
			{
				return !wcsncmp(*GetTypeName(), L"Affector_", 9);
			}

			bool IsBuilding()
			{
				return !wcsncmp(*GetTypeName(), L"Building_", 9);
			}

			bool IsLight()
			{
				return !wcsncmp(*GetTypeName(), L"Light_", 6);
			}

			bool IsProp()
			{
				return !wcsncmp(*GetTypeName(), L"Prop_", 5);
			}

			bool IsProjectile()
			{
				return !wcsncmp(*GetTypeName(), L"Projectile_", 11);
			}
		};

	}
}