#include "stdafx.h"
#include "DirectX9.h"


namespace HonSDK
{
	namespace Native
	{
		LPDIRECT3DDEVICE9 m_pD3Ddev = nullptr;

		DWORD dwEndscene_hook, dwEndscene_ret;

		BYTE EndSceneOpCodes[6];

		MAKE_HOOK<convention_type::stdcall_t, int, LPDIRECT3DDEVICE9> Game_EndScene;

		__declspec(naked) void MyEndscene()
		{
			__asm
			{
				mov dword ptr ss : [ebp - 10], esp;
				mov esi, dword ptr ss : [ebp + 0x8];    //replace patched code
				mov m_pD3Ddev, esi;            //Get the device
			}


			__asm
			{
				jmp dwEndscene_ret;                    //jump back to normal endscene
			}

		}

		HonSDK::Native::DirectX9 * HonSDK::Native::DirectX9::GetInstance()
		{
			static auto* instance = new Native::DirectX9();
			return instance;
		}

		bool HonSDK::Native::DirectX9::ApplyHooks() const
		{
			DWORD hD3D = NULL;
			DWORD * VTable;

			while (!hD3D) hD3D = (DWORD)Memory::lGetModuleHandle(L"d3d9.dll");
			DWORD PPPDevice = Memory::FindPattern(hD3D, 0x128000, (PBYTE)"\xC7\x06\x00\x00\x00\x00\x89\x86\x00\x00\x00\x00\x89\x86", "xx????xx????xx");
			memcpy(&VTable, (VOID *)(PPPDevice + 2), 4);

			dwEndscene_hook = VTable[42];

			Console::PrintLn("dwEndscene_hook: %#010x", dwEndscene_hook);
			Game_EndScene.Apply((DWORD)dwEndscene_hook, [](LPDIRECT3DDEVICE9 device) -> int
			{
				__asm pushad;
				m_pD3Ddev = device;
				EventHandler<3, OnEndScene>::GetInstance()->TriggerProcess();
				//Drawing::DrawFontText(50, 50, D3DCOLOR_XRGB(0, 0xff, 0), "HonFuse", 15);

				//Drawing::DrawLine(50, 50, 10000, 10000, 2, D3DCOLOR_XRGB(0xff, 0, 0));

				__asm popad;
				return Game_EndScene.CallOriginal(device);
			});
			return Game_EndScene.IsApplied();
		}

		LPDIRECT3DDEVICE9 DirectX9::GetDevice()
		{
			return m_pD3Ddev;
		}

	}
}