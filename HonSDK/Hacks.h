#pragma once
#include <Windows.h>
#include "Core.h"

namespace HonSDK
{
	namespace Native
	{
		class Hacks
		{
		public:
			static void EnableZoomHack();

		private:
			static void Patch(void * address, char* bytes, int len);
		};
	}
}