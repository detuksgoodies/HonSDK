#pragma once
#include <datetimeapi.h>

namespace HonSDK
{
	namespace Native
	{
		class GameEvent
		{
		public:
			int cgameevent0;
			DWORD dword4;
			WORD Flag;
			BYTE gapA[2];
			DWORD Expire;
			WORD word10;
			BYTE gap12[2];
			DWORD SourceEntity;
			DWORD PositionX;
			DWORD PositionY;
			DWORD PositionZ;
			DWORD dword24;
			DWORD dword28;
			DWORD dword2C;
			DWORD Angles;
			DWORD dword34;
			DWORD dword38;
			DWORD Scale;
			DWORD EffectScale;
			DWORD TargetEntity;
			DWORD TargetPos;
			DWORD dword4C;
			DWORD dword50;
			DWORD dword54;
			DWORD dword58;
			DWORD dword5C;
			DWORD TargetAngles;
			DWORD dword64;
			DWORD dword68;
			DWORD TargetScale;
			DWORD TargetEffectScale;
			DWORD EffectId;
			BYTE byte78;
			BYTE gap79[3];
			DWORD Sound;
			DWORD EffectThread;
			BYTE byte84;
			BYTE byte85;
			BYTE byte86;
		};
	}
}
