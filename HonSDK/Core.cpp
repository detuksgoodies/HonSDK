#include "stdafx.h"

#include "Detour.hpp"
#include "Core.h"
#include "Console.h"
#include "Hacks.h"
#include "EventHandler.h"
#include "Game.h"
#include "DirectX9.h"
#include "Packet.h"

#define APPLY_HOOKS

namespace HonSDK
{
	namespace Native
	{
		template <int uniqueEventNumber, typename T, typename ... TArgs>
		EventHandler<uniqueEventNumber, T, TArgs...>* EventHandler<uniqueEventNumber, T, TArgs...>::instance = nullptr;

		HMODULE Core::hCGame = (HMODULE)nullptr;
		HMODULE Core::hGame = (HMODULE)nullptr;
		HMODULE Core::hK2 = (HMODULE)nullptr;
		HMODULE Core::hGameShared = (HMODULE)nullptr;
		
		Core::Core()
		{
			InitHandles();
#ifdef APPLY_HOOKS
			if(!ApplyHooks())
			{
				Console::PrintLn("------>Hook apply failed!!<------");
				return;
			}
#endif
			Hacks::EnableZoomHack();

			EventHandler<1, OnWndProc, HWND, UINT, WPARAM, LPARAM>::GetInstance()->Add(nullptr);
			EventHandler<1, OnWndProc, HWND, UINT, WPARAM, LPARAM>::GetInstance()->Remove(nullptr);
			EventHandler<2, OnClientSync>::GetInstance()->Add(nullptr);
			EventHandler<2, OnClientSync>::GetInstance()->Remove(nullptr);
			EventHandler<3, OnEndScene>::GetInstance()->Add(nullptr);
			EventHandler<3, OnEndScene>::GetInstance()->Remove(nullptr);
			EventHandler<4, OnMainLoop>::GetInstance()->Add(nullptr);
			EventHandler<4, OnMainLoop>::GetInstance()->Remove(nullptr);
			EventHandler<5, OnSendGameData, Buffer*, bool>::GetInstance()->Add(nullptr);
			EventHandler<5, OnSendGameData, Buffer*, bool>::GetInstance()->Remove(nullptr);
			EventHandler<6, OnReceivePacket, Packet*, int>::GetInstance()->Add(nullptr);
			EventHandler<6, OnReceivePacket, Packet*, int>::GetInstance()->Remove(nullptr);
			EventHandler<7, OnGameEventSpawn, GameEvent*>::GetInstance()->Add(nullptr);
			EventHandler<7, OnGameEventSpawn, GameEvent*>::GetInstance()->Remove(nullptr);
			EventHandler<8, OnGameEntityCreate, GameEntity*>::GetInstance()->Add(nullptr);
			EventHandler<8, OnGameEntityCreate, GameEntity*>::GetInstance()->Remove(nullptr);
			EventHandler<9, OnGameEntityDelete, int>::GetInstance()->Add(nullptr);
			EventHandler<9, OnGameEntityDelete, int>::GetInstance()->Remove(nullptr);
		}


		Detour * Core::get_DetourInstance()
		{
			static auto instance = new Detour();
			return instance;
		}

		bool Core::InitHandles()
		{
			Console::PrintLn("---------------------------------------------------------");
			Console::PrintLn("Core: Collecting handles ProdID: %d", GetCurrentProcessId());
			MODULEENTRY32 me32;

			// Set the size of the structure before using it.
			me32.dwSize = sizeof(MODULEENTRY32);

			auto hModuleSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, GetCurrentProcessId());
			if (hModuleSnap == NULL)
			{
				//cout << "Handles: hModuleSnap Null " << GetLastError() << endl;
				return false;
			}
			if (!Module32First(hModuleSnap, &me32))
			{
				//cout << "Handles: Module32First Null " << GetLastError() << endl;
				return false;
			}

			do
			{
				if (wcscmp(L"game_shared.dll", me32.szModule) == 0)
					hGameShared = (HMODULE)me32.modBaseAddr;
				if (wcscmp(L"game.dll", me32.szModule) == 0)
					hGame = (HMODULE)me32.modBaseAddr;
				if (wcscmp(L"k2.dll", me32.szModule) == 0)
					hK2 = (HMODULE)me32.modBaseAddr;
				if (wcscmp(L"cgame.dll", me32.szModule) == 0)
					hCGame = (HMODULE)me32.modBaseAddr;
			} while (Module32Next(hModuleSnap, &me32));

			Console::PrintLn("Core: game_shared.dll: %#010x", hGameShared);
			Console::PrintLn("Core: game.dll: %#010x", hGame);
			Console::PrintLn("Core: k2.dll: %#010x", hK2);
			Console::PrintLn("Core: cgame.dll: %#010x", hCGame);
			return true;
		}

		bool Core::ApplyHooks() const
		{
			VERIFY_HOOK(Game::GetInstance()->ApplyHooks, "Game");
			VERIFY_HOOK(DirectX9::GetInstance()->ApplyHooks, "Drawing");
			return true;
		}

	}
}
