#include "Action.h"
#include "Game.h"
#include "Packets.h"

void HonSDK::Native::Action::Move(float x, float y, char flag)// default 0x2
{
	Buffer buffer;
	PacketBuilder::CreateMovePacket(&buffer, x, y, flag);
	Game::GetInstance()->SendGameData(&buffer, false);
}

void HonSDK::Native::Action::Hold()
{
	Buffer buffer;
	PacketBuilder::CreateHoldPacket(&buffer);
	Game::GetInstance()->SendGameData(&buffer, false);
}

void HonSDK::Native::Action::Attack(GameEntity* entity, char flag)// default 0x9 (hit) or 0x8 (move hit)
{
	Buffer buffer;
	PacketBuilder::CreateAttackPacket(&buffer, entity, flag);
	Game::GetInstance()->SendGameData(&buffer, false);

}

void HonSDK::Native::Action::SelectEntities(GameEntity** entities, int count, char flag)// default 0x1
{
	Buffer buffer;
	PacketBuilder::CreateSelectUnitsPacket(&buffer, entities, count, flag);
	Game::GetInstance()->SendGameData(&buffer, false);
}

void HonSDK::Native::Action::SelectEntity(GameEntity* entity, char flag)// default 0x1
{
	Buffer buffer;
	GameEntity* entities[1];
	entities[0] = entity;
	PacketBuilder::CreateSelectUnitsPacket(&buffer, entities, 1, flag);
	Game::GetInstance()->SendGameData(&buffer, false);
}

void HonSDK::Native::Action::CastSpell(GameEntity* entity, char slot)
{
	Buffer buffer;
	PacketBuilder::CreateCastSpellPacket(&buffer, entity, slot);
	Game::GetInstance()->SendGameData(&buffer, false);
}

void HonSDK::Native::Action::CastSpell(GameEntity* entity, char slot, float x, float y)
{
	Buffer buffer;
	PacketBuilder::CreateCastSpellPacket(&buffer, entity, slot, x, y);
	Game::GetInstance()->SendGameData(&buffer, false);
}

void HonSDK::Native::Action::CastSpell(GameEntity* entity, char slot, GameEntity* target)
{
	Buffer buffer;
	PacketBuilder::CreateCastSpellPacket(&buffer, entity, slot, target);
	Game::GetInstance()->SendGameData(&buffer, false);
}

void HonSDK::Native::Action::CastSpell(GameEntity* entity, char slot, float fromX, float fromY, float toX, float toY)
{
}
