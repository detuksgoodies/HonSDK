#pragma once
#pragma once
#include "Macros.h"

namespace HonSDK
{
	namespace Native
	{
		// This is only used by server :/
		class DLLEXPORT Buffer
		{
		public:
			int VirtualTable;
			unsigned char* Data;
			int AllocatedSize;
			int Size;
			int CurrentOffset;
			int SomeFlag;

			//MAKE_GET(int*, VirtualTable, 0x0);
			//MAKE_GET_PTR(char*, DataPtr, 0x4);
			//MAKE_GET(int, AllocatedSize, 0x8);
			//MAKE_GET(int, Size, 0xC);
			//MAKE_GET(int, CurrentOffset, 0x10);
			//MAKE_GET(int, SomeFlag, 0x14);

			Buffer()
			{
				
			}
		};
	}
}