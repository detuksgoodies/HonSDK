// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#include "ObjectManager.h"
#include "Console.h"
#include "Core.h"
#include "Detour.hpp"
#include "ObjectManagerTests.h"
#include "EventHandler.h"
#include "Packets.h"
#include "Camera.h"
#include "GameInternal.h"
#include "Exports.h"
#include "Bootstrapper.h"

#include <set>
#include "Drawing.h"

using namespace HonSDK::Native;

bool OnPacketTest(Buffer* buffer, bool type)
{
	__try
	{
		if (buffer == nullptr)
			return true;
		int size = buffer->Size;
		unsigned char* data = buffer->Data;
		int maxOffset = buffer->AllocatedSize;

		Console::PrintLn("Client>>>: GameDataSend: sent with size: %d ", size);

		for (int i = 0; i < size; i++)
		{
			Console::Print("%02x ", data[i]);
		}
		Console::PrintLn();

		//PacketBuilder::CreateMovePacket(buffer, 1000, 1000);
		//if (size > 30)
		//	return false;
		return true;
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		Console::PrintLn("Exception: ");
	}
}
std::set<int> ignore;

bool OnReceivePacketTest(Packet* packet, int smth)
{
	if (packet == nullptr)
		return true;
	int size = packet->Size;

	int header = packet->Data[0];
	if (ignore.find(header) != ignore.end())
		return true;

	if (size > 10 && packet->Data[8] == 0x5a && packet->Data[9] == 0x21)
	{
		Console::PrintLn(">>>Client: ReceivedPacket: %d with size: %d ", header, size);
		return false;
	}
	if (size > 10 && packet->Data[8] == 0x5a)
	{
		//if(packet->Data[9] == 0x0b || packet->Data[9] == 0x12 || packet->Data[9] == 0x13 ||
		//	packet->Data[9] == 0x10 || packet->Data[9] == 0x11)
		if (packet->Data[9] < 0x20)
			return true;
	}

	Console::PrintLn(">>>Client: ReceivedPacket: %d with size: %d ", header, size);

	for (int i = 0; i < size; i++)
	{
		Console::Print("%02x ", packet->Data[i]);
	}
	Console::PrintLn();
	//if (header == 0x35)
	//	return false;
	//PacketBuilder::CreateMovePacket(buffer, 1000, 1000);
	//if (size > 30)
	//	return false;
	return true;
}

bool OnGameEventTest(GameEvent* gameEvent)
{
	if (gameEvent == nullptr)
		return true;

	Console::PrintLn("##GameEvent: Source %d Target %d", gameEvent->SourceEntity, gameEvent->TargetEntity);
	return true;

}
bool OnEntityDeleteTest(int netId)
{

	Console::PrintLn("##EntityDelte: netID %d ", netId);
	return true;

}

bool OnEntityCreateTest(GameEntity* entity)
{
	if (entity == nullptr)
		return true;

	Console::PrintLn("##EntityCreate: type %S netId: %d %#010x", *entity->GetTypeName(), *entity->GetNetworkId(), entity);
	return true;

}

bool OnEndSceneTest()
{
	std::vector<GameEntity*> entities;
	ObjectManager::GetEntities(&entities);
	auto camera = GameInternal::GetInstance()->GetGameCamera();
	if (camera == nullptr)
		return true;
	Vector2f posScreen;
	Vector3f posWorld;
	for (GameEntity* entity : entities)
	{
		//auto myHero = ObjectManager::GetMyHero();
		if(!entity->IsHero() && !entity->IsCreep())
			continue;
		auto unit = (UnitEntity*)entity;
		posWorld = *unit->GetPos();

		camera->WorldToScreen(&posWorld, &posScreen);
		Drawing::DrawFontText(posScreen.GetX(), posScreen.GetY(), D3DCOLOR_XRGB(0, 0xff, 0), "Hello", 5);
	}
	return true;

}

void Initialize()
{
	Console::Create();
	Console::Show();
	auto _core = new Core();

	//ObjectManagerTests::TestEntitiesCount();
	//ObjectManagerTests::TestEntitiesOutput();
	EventHandler<5, OnSendGameData, Buffer*, bool>::GetInstance()->Add(OnPacketTest);
	//EventHandler<9, OnGameEntityDelete, int>::GetInstance()->Add(OnEntityDeleteTest);
	//EventHandler<8, OnGameEntityCreate, GameEntity*>::GetInstance()->Add(OnEntityCreateTest);
	//EventHandler<7, OnGameEventSpawn, GameEvent*>::GetInstance()->Add(OnGameEventTest);

	Console::PrintLn("Camera: %#010x ", Camera::GetInstance());
	Console::PrintLn("MyPlayer: %#010x ", GameInternal::GetInstance()->GetCurrentPlayer());
	Console::PrintLn("Camera W2S: %#010x ", Exports::GetInstance()->WorldToSceen);
	//EventHandler<3, OnEndScene>::GetInstance()->Add(OnEndSceneTest);

	//ignore.insert(0x5f);

	Bootstrapper::GetInstance()->Initialize();

}

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  reason,
	LPVOID lpReserved
)
{
	static HANDLE hThread = nullptr;

	if (reason == DLL_PROCESS_ATTACH)
	{
		hThread = CreateThread(nullptr, NULL, reinterpret_cast<LPTHREAD_START_ROUTINE>(Initialize), nullptr, NULL, nullptr);
	}

	if (reason == DLL_PROCESS_DETACH)
	{
		SuspendThread(hThread);
	}

	return TRUE;
}



