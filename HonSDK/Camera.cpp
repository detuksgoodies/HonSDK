#include "Camera.h"
#include "Exports.h"

bool HonSDK::Native::Camera::WorldToScreen(Vector3f* position, Vector2f* pointOut)
{
	return ((bool(__fastcall*)(Camera*, int, Vector3f*, Vector2f*))Exports::GetInstance()->WorldToSceen)(this, 0, position, pointOut);
}
