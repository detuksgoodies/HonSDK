#pragma once
#include "Macros.h"
#include "GameEntity.h"
#include "ClientEntity.h"
#include <vector>
#include "UnitEntity.h"
#include "HeroEntity.h"

namespace HonSDK
{
	namespace Native
	{
		class DLLEXPORT ObjectManager
		{
		public:
			static GameEntity** GetEntityArray();
			static ClientEntity* GetClientEntityArray();
			static ClientEntity* GetClientEntity(int index);

			static GameEntity* GetGameEntityByIndex(int index);
			static GameEntity* GetGameEntityByNetworkId(int networkId);

			static HeroEntity* GetMyHero();

			static void GetHeroEntities(std::vector<UnitEntity*> *entities);
			static void GetEntities(std::vector<GameEntity*> *entities);

			static int GetEntityWrapperArraySize();

		};
	}
}
