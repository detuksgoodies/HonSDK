#pragma once
#include <math.h>
#include <random>
#include "Macros.h"

namespace HonSDK
{
	namespace Native
	{
		class DLLEXPORT Vector2f
		{
		protected:
			float X, Y;
		public:
			Vector2f();
			Vector2f(float xx, float yy);

			Vector2f SwitchYZ() const;
			bool IsValid() const;
			operator float*();

			float GetX() const;
			float GetY() const;
			float DistanceTo(const Vector2f & v) const;

			Vector2f Randomize();
		};
	}
}