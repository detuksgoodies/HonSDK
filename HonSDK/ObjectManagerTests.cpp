#include "stdafx.h"
#include "ObjectManagerTests.h"
#include "ObjectManager.h"
#include "Console.h"
#include "GameInternal.h"
#include "UnitEntity.h"

namespace HonSDK
{
	namespace Native
	{

		void HonSDK::Native::ObjectManagerTests::TestEntitiesCount()
		{
			Console::PrintLn("ClientEntityArraySize: %d", ObjectManager::GetEntityWrapperArraySize());
		}

		void HonSDK::Native::ObjectManagerTests::TestEntitiesOutput()
		{
			__try
			{
				for (int i = 0; i < ObjectManager::GetEntityWrapperArraySize(); i++)
				{
					auto clientEntity = ObjectManager::GetClientEntity(i);
					//Console::PrintLn("clientEntity: %#010x", clientEntity);
					if (clientEntity == nullptr)
						continue;
					auto gameEntity = clientEntity->GetEntity();
					//Console::PrintLn("EntityNr %d", i);
					//Console::PrintLn("gameEntity: %#010x", gameEntity);
					if (gameEntity == nullptr)
						continue;
					if(!gameEntity->IsHero())
						continue;
					auto unit = (UnitEntity*)gameEntity;
					Console::PrintLn("EntityNr %d", i);
					Console::PrintLn("NetworkId: %d", *unit->GetNetworkId());
					Console::PrintLn("gameEntity: %#010x", gameEntity);
				}
	
			}
			__except (EXCEPTION_EXECUTE_HANDLER)
			{
				Console::PrintLn("Exception: ");
			}
		}

		void IterateBTreeCount(BTreeNode<int> *node)
		{
			if (node == nullptr || node->IsLastElement)
				return;
			Console::PrintLn("Node index: %d", node->Value);
			auto clientEntity = ObjectManager::GetClientEntity(node->Value);
			Console::PrintLn("Client entity offset: %#010x", clientEntity);
			Console::PrintLn("Client entity unit offset: %#010x", clientEntity->GetEntity());
			IterateBTreeCount(node->LeftNode);
			IterateBTreeCount(node->RightNode);
		}

		void HonSDK::Native::ObjectManagerTests::TestEntitiesTree()
		{
			__try
			{
				auto game = GameInternal::GetInstance();
				Console::PrintLn("game: %#010x", game);
				if (game == nullptr)
					return;
				auto entityManager = game->GetGameEntityManager();
				Console::PrintLn("EntityManager: %#010x", entityManager);
				if(entityManager == nullptr)
					return;
				auto tree = entityManager->GetClientEntityTree();
				Console::PrintLn("tree: %#010x", tree);
				if (tree == nullptr)
					return;
				auto rootNode = tree->RootNode;
				Console::PrintLn("rootNode: %#010x", rootNode);
				if (rootNode == nullptr)
					return;
				auto parent = rootNode->ParentNode;
				Console::PrintLn("parent: %#010x", parent);
				if (parent == nullptr)
					return;
				IterateBTreeCount(parent);
			}
			__except (EXCEPTION_EXECUTE_HANDLER)
			{
				return;
			}
		}

	}
}