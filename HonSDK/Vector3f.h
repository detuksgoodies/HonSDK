#pragma once
#include <math.h>
#include <random>
#include "Macros.h"

namespace HonSDK
{
	namespace Native
	{
		class DLLEXPORT Vector3f
		{
		public:
			float X, Y, Z;
			Vector3f();
			Vector3f(float xx, float yy, float zz);

			Vector3f SwitchYZ() const;
			bool IsValid() const;
			operator float*();

			float GetX() const;
			float GetY() const;
			float GetZ() const;
			float DistanceTo(const Vector3f & v) const;

			Vector3f Randomize();
		};
	}
}