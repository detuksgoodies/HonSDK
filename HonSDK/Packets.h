#pragma once
#include "Buffer.h"
#include "ByteBuffer.h"
#include "GameEntity.h"


namespace HonSDK
{
	namespace Native
	{

		class PacketBuilder
		{
		public:
			static void CreateMovePacket(Buffer* buffer, float x, float y, char flag);
			static void CreateHoldPacket(Buffer * buffer);
			static void CreateSelectUnitsPacket(Buffer * buffer, GameEntity** entieies, int count, char flag);
			static void CreateAttackPacket(Buffer* buffer, GameEntity* entity, char flag);
			static void CreateCastSpellPacket(Buffer* buffer, GameEntity* entity, char slot, float x, float y);
			static void CreateCastSpellPacket(Buffer * buffer, GameEntity * entity, char slot);
			static void CreateCastSpellPacket(Buffer * buffer, GameEntity * entity, char slot, GameEntity * target);
		private:
			static ByteBuffer DateBuffer;
		};
	}
}
