#pragma once
#include "Macros.h"
#include "GameEntity.h"

#define ENTITY_WRAPPER_SIZE 0x690

namespace HonSDK
{
	namespace Native
	{
		class DLLEXPORT ClientEntity
		{
		public:
			MAKE_GET_PTR(GameEntity, Entity, 0x654);
			
		};

	}
}
