#include "stdafx.h"
#include "Macros.h"
#include <cstdio>
#include "Console.h"

namespace HonSDK
{
	namespace Native
	{
		void Console::Create()
		{
			char title[56];
			sprintf_s(title, "Debug Window - %d", GetCurrentProcessId());

			AllocConsole();
			SetConsoleTitleA(title);
			freopen("CON", "w", stdout);
		}

		void Console::PrintLn()
		{
			auto hConsole = GetHandle();
			if (hConsole != nullptr)
			{
				printf("\r\n");
			}
		}

		void Console::PrintLn(const char* fmt, ...)
		{
			auto hConsole = GetHandle();
			char buffer[512];

			if (hConsole != nullptr)
			{
				//Process message
				va_list argList;
				va_start(argList, fmt);
				vsprintf_s(buffer, fmt, argList);
				va_end(argList);

				printf("[HonSDK] %s\r\n", buffer);
			}
		}

		void Console::Print(const char * format, ...)
		{
			auto hConsole = GetHandle();
			char buffer[512];

			if (hConsole != nullptr)
			{
				//Process message
				va_list argList;
				va_start(argList, format);
				vsprintf_s(buffer, format, argList);
				va_end(argList);
				printf("%s", buffer);
			}
		}

		void Console::Show()
		{
			auto hConsole = GetHandle();

			if (hConsole != nullptr)
			{
				ShowWindow(hConsole, SW_SHOW);
				SetForegroundWindow(hConsole);
			}
		}

		void Console::Hide()
		{
			auto hConsole = GetHandle();

			if (hConsole != nullptr)
			{
				ShowWindow(hConsole, SW_HIDE);
			}
		}

		HWND Console::GetHandle()
		{
			return GetConsoleWindow();
		}

	}
}
