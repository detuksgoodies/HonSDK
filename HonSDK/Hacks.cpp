#include "Hacks.h"
#include "Console.h"

//#define DEBUG_INFO

void HonSDK::Native::Hacks::EnableZoomHack()
{
	int ZoomOutAdr = (int)GetProcAddress(Core::hGameShared, "?ZoomOut@CPlayer@@QAEXXZ");
#ifdef DEBUG_INFO
	Console::PrintLn("CPlayer::ZoomOut: %#010x", ZoomOutAdr);
#endif
	if (!ZoomOutAdr)
		return;
	//ZoomOut +74 nOp nop
	void* patchAddress1 = (void*)(ZoomOutAdr + 0x74);
	char* patchBytes1 = "\x90\x90";
	Patch(patchAddress1, patchBytes1, 2);

	int PrepareClientState = (int)GetProcAddress(Core::hGameShared, "?PrepareClientState@CPlayer@@QAEXAAVCClientState@@_N@Z");
#ifdef DEBUG_INFO
	Console::PrintLn("CPlayer::PrepareClientState: %#010x", ZoomOutAdr);
#endif
	if (!PrepareClientState)
		return;
	//PrepareClientState +3E9 nOp nop
	void* patchAddress2 = (void*)(PrepareClientState + 0x3E9);
	char* patchBytes2 = "\x90\x90\x90\x90\x90\x90\x90\x90";
	Patch(patchAddress2, patchBytes2, 8);

	//PrepareClientState +3D6 Jump!
	void* patchAddress3 = (void*)(PrepareClientState + 0x3D6);
	char* patchBytes3 = "\xEB";
	Patch(patchAddress3, patchBytes3, 1);

	//PrepareClientState +41B NopNOp!
	void* patchAddress4 = (void*)(PrepareClientState + 0x41B);
	char* patchBytes4 = "\x90\x90\x90\x90\x90\x90\x90\x90";
	Patch(patchAddress4, patchBytes4, 8);

	int SetupCamera = (int)GetProcAddress(Core::hGameShared, "?SetupCamera@CPlayer@@QAEXAAVCCamera@@ABV?$CVec3@M@@1@Z");
#ifdef DEBUG_INFO
	Console::PrintLn("CPlayer::SetupCamera: %#010x", ZoomOutAdr);
#endif
	if (!SetupCamera)
		return;
	//SetupCamera+1C7 Jump
	void* patchAddress5 = (void*)(SetupCamera + 0x1C7);
	char* patchBytes5 = "\xEB";
	Patch(patchAddress5, patchBytes5, 1);
}

void HonSDK::Native::Hacks::Patch(void * address, char* bytes, int len)
{
	DWORD curProtection;
	VirtualProtect(address, len, PAGE_EXECUTE_READWRITE, &curProtection);

	memcpy(address, bytes, len);

	DWORD temp;

	VirtualProtect(address, len, curProtection, &temp);
}
