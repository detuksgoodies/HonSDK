#pragma once
#include <vector>
#include "GameEntity.h"


namespace HonSDK
{
	namespace Native
	{
		class DLLEXPORT Action
		{
		public:
			static void SelectEntities(GameEntity** entities, int count, char flag = 0x1);
			static void SelectEntity(GameEntity * entity, char flag);
			static void Move(float x, float y, char flag = 0x2);
			static void Hold();
			static void Attack(GameEntity* entity, char flag = 0x8);
			static void CastSpell(GameEntity* entity, char slot);
			static void CastSpell(GameEntity* entity, char slot, float x, float y);
			static void CastSpell(GameEntity* entity, char slot, GameEntity* target);
			static void CastSpell(GameEntity* entity, char slot, float fromX, float fromY, float toX, float toY);
		};
	}
}
