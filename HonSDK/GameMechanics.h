#pragma once
#include "Exports.h"

namespace HonSDK
{
	namespace Native
	{
		class DLLEXPORT GameMechanics
		{
		public:
			double GetArmorDamageAdjustment(int armorPercentage, float armor);
		};
	}
}
