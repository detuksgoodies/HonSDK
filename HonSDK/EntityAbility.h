#pragma once
#include "Macros.h"
#include "Exports.h"

namespace HonSDK
{
	namespace Native
	{
		class DLLEXPORT EntityAbility
		{
		public:
			MAKE_GET_PTR(wchar_t*, AbilityName, 0x20);
			MAKE_GET(int, AbilityLevel, 0x84);

			IMPLVTFUNC(float, GetManaCost, 0x52C);

			bool IsReady()
			{
				return ((bool(__fastcall*)(EntityAbility*, int))Exports::GetInstance()->IsReady)(this, 0);
			}

			int GetActualRemainingCooldownTime()
			{
				return ((int(__fastcall*)(EntityAbility*, int))Exports::GetInstance()->GetActualRemainingCooldowntime)(this, 0);
			}

			int GetRemainingCooldownTime(bool smth)
			{
				return ((int(__fastcall*)(EntityAbility*, int, bool))Exports::GetInstance()->GetRemainingCooldowntime)(this, 0, smth);
			}

			int GetCooldownEndTime()
			{
				return ((int(__fastcall*)(EntityAbility*, int))Exports::GetInstance()->GetCooldownEndTime)(this, 0);
			}

			int GetDynamicRange(bool smth)
			{
				return ((bool(__fastcall*)(EntityAbility*, int, bool))Exports::GetInstance()->GetDynamicRange)(this, 0, smth);
			}

		};
	}
}
