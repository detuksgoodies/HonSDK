#include "Bootstrapper.h"
#include "Console.h"
#include <xstring>

namespace HonSDK
{
	namespace Native
	{
		LPCWSTR Bootstrapper::GetDllPath(char * dll)
		{
			char buffer[MAX_PATH];
			GetModuleFileNameA(reinterpret_cast<HINSTANCE>(&__ImageBase), buffer, MAX_PATH);

			auto pos = std::string(buffer).find_last_of("\\/");

			// Path to directory
			char dir[MAX_PATH];
			//strncpy_s( dir, buffer, strlen( buffer )   );
			strncpy_s(dir, buffer, pos);

			// Path to directory with dll attached
			char dirDllPath[MAX_PATH];
			sprintf_s(dirDllPath, "%s\\%s", dir, dll);

			// Convert to wchar
			auto wcharDLLPath = new wchar_t[MAX_PATH];
			MultiByteToWideChar(CP_ACP, 0, dirDllPath, -1, wcharDLLPath, 4096);

			return wcharDLLPath;
		}
		Bootstrapper * Bootstrapper::GetInstance()
		{
			static auto* instance = new Bootstrapper();
			return instance;
		}
		bool Bootstrapper::Initialize()
		{
			__try
			{
				if (!this->LoadMemoryLayout())
				{
					Console::PrintLn("[!] Failed to load memory layout, falling back on paths.");
					m_isFallback = true;
				}
				else
				{
					m_isFallback = false;
				}

				if (!this->InjectWrapper())
				{
					Console::PrintLn("[!] Failed to inject HonSDK.Wrapper.dll, error: 0x%08x", GetLastError());
					return false;
				}

				if (!this->HostClr())
				{
					Console::PrintLn("[!] Failed to host CLR, error: 0x%08x", GetLastError());
					return false;
				}

				return this->LoadSandbox();
			}
			__except (1)
			{
				Console::PrintLn("[!] Bootstraper exception, error: 0x%08x", GetLastError());
				return false;
			}
		}

		bool Bootstrapper::LoadMemoryLayout()
		{
			__try
			{
				auto hMapFile = OpenFileMappingA(FILE_MAP_READ, FALSE, "Local\\HonFuse");

				if (hMapFile != nullptr)
				{
					auto pMemoryFile = static_cast<BootstrapMemoryLayout*>(MapViewOfFile(hMapFile, FILE_MAP_READ, 0, 0, 1024));

					if (pMemoryFile != nullptr)
					{
						auto lpMemoryLayout = malloc(sizeof(BootstrapMemoryLayout));
						memcpy(lpMemoryLayout, pMemoryFile, sizeof(BootstrapMemoryLayout));
						m_bsMemoryLayout = static_cast<BootstrapMemoryLayout*>(lpMemoryLayout);

						UnmapViewOfFile(pMemoryFile);
						CloseHandle(hMapFile);

						return true;
					}

					Console::PrintLn("[!] MapViewOfFile error: 0x%08x", GetLastError());
					CloseHandle(hMapFile);
				}
				else
				{
					Console::PrintLn("[!] OpenFileMapping error: 0x%08x", GetLastError());
				}
			}
			__except (1)
			{
				Console::PrintLn("[!] FileMapping exception, error: 0x%08x", GetLastError());
				return false;
			}

			return false;
		}

		void Bootstrapper::Trigger(BootstrapEventType eventType)
		{
			switch (eventType)
			{
			//case BootstrapEventType::Load:
			//	if (ClientFacade::GetInstance()->GetGameState() == static_cast<int>(GameMode::Running))
			//		EventHandler<3, OnGameStart>::GetInstance()->Trigger();
			//
			//	if (ClientFacade::GetInstance()->GetGameState() == static_cast<int>(GameMode::Connecting))
			//		EventHandler<26, OnGameLoad>::GetInstance()->Trigger();
			//	break;
			}
		}

		bool Bootstrapper::HostClr()
		{
			CLRCreateInstance(CLSID_CLRMetaHost, IID_ICLRMetaHost, reinterpret_cast<LPVOID*>(&this->pMetaHost));
			this->pMetaHost->GetRuntime(L"v4.0.30319", IID_PPV_ARGS(&pRuntimeInfo));
			pRuntimeInfo->GetInterface(CLSID_CLRRuntimeHost, IID_PPV_ARGS(&this->pRuntimeHost));

			auto hr = this->pRuntimeHost->Start();

			return SUCCEEDED(hr);
		}

		bool Bootstrapper::InjectWrapper()
		{
			auto path = m_isFallback
				? GetDllPath("HonSDK.Wrapper.dll")
				: m_bsMemoryLayout->HonSDKWrapperDllPath;

			return LoadLibraryW(path) != nullptr;
		}

		bool Bootstrapper::LoadSandbox()
		{
		
			auto path = m_isFallback
				? GetDllPath("HonSDK.Sandbox.dll")
				: m_bsMemoryLayout->SandboxDllPath;
		
			DWORD dwRetCode = 0;
			auto hr = this->pRuntimeHost->ExecuteInDefaultAppDomain(
				path,
				L"HonSDK.Sandbox.Sandbox",
				L"Bootstrap",
				L"HonSDK.Sandbox.exe",
				&dwRetCode
			);
		
			if (FAILED(hr))
			{
				Console::PrintLn("[!] Sandbox failed: %08x", hr);
			}
		
			return SUCCEEDED(hr);
		}

		void Bootstrapper::SetMemoryLayout(BootstrapMemoryLayout* layout)
		{
			this->m_bsMemoryLayout = layout;
		}

		BootstrapMemoryLayout* Bootstrapper::GetMemoryLayout()
		{
			return this->m_bsMemoryLayout;
		}
	}
}

