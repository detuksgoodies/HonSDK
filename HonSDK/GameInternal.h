#pragma once
#include "Macros.h"
#include "Core.h"
#include "Offsets.h"
#include "EntityManager.h"
#include "Player.h"
#include "Vector3f.h"
#include "GameMechanics.h"

namespace HonSDK
{
	namespace Native
	{
		class Camera;
		struct HostClient
		{
			
		};

		struct GameInfo
		{
			MAKE_GET(int, GameState, 0x84);
		};

		class DLLEXPORT CursorContainer
		{
		public:
			MAKE_GET(Vector3f, CursorPos, 0x60);
		};

		class DLLEXPORT GameInternal
		{
		public:
			static GameInternal* GetInstance()
			{
				return *reinterpret_cast<GameInternal**>((DWORD)Core::hCGame + static_cast<__int32>(Offsets::Global::GameInternal));
			}

			MAKE_GET(int, GameStartedPtr, 0x1E);

			MAKE_GET_PTR(GameInfo, Info, 0x1E);
			MAKE_GET_PTR(GameMechanics, Mechanics, 0x80);
			MAKE_GET_PTR(BTree<Player*>, PlayerTree, 0x164);
			MAKE_GET_PTR(HostClient, GameHostClient, 0x220);
			MAKE_GET_PTR(EntityManager, GameEntityManager, 0x224);
			MAKE_GET_PTR(Camera, GameCamera, 0x22C);
			MAKE_GET_PTR(CursorContainer, GameCursorContainer, 0x3F4);
			MAKE_GET_PTR(Player, CurrentPlayer, 0x3EC);

			bool IsPlaying()
			{
				int state = *GetInfo()->GetGameState();
				return *GetGameStartedPtr() != 0 && (state == 5 || state == 6 || state == 7 || state == 8);
			}
		};

	}
}
