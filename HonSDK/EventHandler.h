#pragma once
#include "Macros.h"
#include <datetimeapi.h>
#include <wincon.h>
#include <functional>
#include <vector>
#include "Buffer.h"
#include "Packet.h"
#include "GameEvent.h"
#include "GameEntity.h"

#define EVENT_TIMEOUT_EJECT 250

namespace HonSDK
{
	namespace Native
	{
		//Game
		typedef bool(OnWndProc)(HWND, unsigned int, WPARAM, LPARAM);
		typedef bool(OnClientSync)();
		typedef int(OnEndScene)();
		typedef bool(OnMainLoop)();
		typedef bool(OnSendGameData)(Buffer* buffer, bool reliable);
		typedef bool(OnReceivePacket)(Packet* buffer, int unkn);
		typedef bool(OnGameEventSpawn)(GameEvent*);

		typedef bool(OnGameEntityCreate)(GameEntity*);
		typedef bool(OnGameEntityDelete)(int netId);


		template <int uniqueEventNumber, typename T, typename ... TArgs> class DLLEXPORT EventHandler
		{
			std::vector<void*> m_EventCallbacks;
			DWORD t_RemovalTickCount;
			static EventHandler* instance;
		public:
			static EventHandler* GetInstance()
			{
				if (instance == nullptr)
				{
					instance = new EventHandler();
				}

				return instance;
			}

			void Add(void* callback)
			{
				if (callback != nullptr)
				{
					m_EventCallbacks.push_back(callback);
				}
			}

			void Remove(void* listener)
			{
				if (listener == nullptr)
				{
					return;
				}

				auto eventPtr = find(m_EventCallbacks.begin(), m_EventCallbacks.end(), listener);
				if (eventPtr != m_EventCallbacks.end())
				{
					m_EventCallbacks.erase(find(m_EventCallbacks.begin(), m_EventCallbacks.end(), listener));
				}

				this->t_RemovalTickCount = GetTickCount();
			}

			bool __cdecl TriggerProcess(TArgs... args)
			{
				auto process = true;
				auto tickCount = GetTickCount();

				for (auto ptr : m_EventCallbacks)
				{
					if (ptr != nullptr)
					{
						if (tickCount - t_RemovalTickCount > EVENT_TIMEOUT_EJECT)
						{
							__try
							{
								if (!static_cast<T*>(ptr) (args...))
								{
									process = false;
								}
							}
							__except (EXCEPTION_EXECUTE_HANDLER)
							{
								Console::PrintLn("Exception: ");
							}
						}
					}
				}

				return process;
			}

			bool __cdecl Trigger(TArgs... args)
			{
				auto tickCount = GetTickCount();

				for (auto ptr : m_EventCallbacks)
				{
					if (ptr != nullptr)
					{
						__try
						{
							if (tickCount - t_RemovalTickCount > EVENT_TIMEOUT_EJECT)
							{
								static_cast<T*>(ptr) (args...);
							}
						}
						__except (EXCEPTION_EXECUTE_HANDLER)
						{
							Console::PrintLn("Exception: ");
						}
					}
				}

				return true;
			}
		};
	}
}
