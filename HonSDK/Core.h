#pragma once
#include <iostream>
#include <Windows.h>
#include <psapi.h>
#include <fcntl.h>
#include <tlhelp32.h>
#include <intrin.h>

#include "Macros.h"

#define VERIFY_HOOK(HOOK, NAME) if (!HOOK ()) { return false; }

namespace HonSDK
{
	namespace Native
	{
		class Detour;

		class DLLEXPORT Core
		{
		public:
			static HMODULE hGameShared;
			static HMODULE hGame;
			static HMODULE hCGame;
			static HMODULE hK2;

			explicit Core();

			static Detour* get_DetourInstance();

		private:
			bool InitHandles();
			bool ApplyHooks() const;
		};
	}
}
