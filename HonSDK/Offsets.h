#pragma once
class Offsets
{
public:

	enum class Global
	{
		GameInternal = 0x2A7D2C,
		SceneManager = 0xB7F2EC,//CSceneManager:: use somewhere

		ClientEntityArray = 0x2ADC80,
		ClientEntityArrayMaxSize = 0x2ADC84,
		ClientEntityArraySize = 0x2ADC88,
	};

	enum class Game
	{
		OnGameSync = 0x55530,
		OnGameUpdate = 0x3EB5C0,// CHost::Execute contains main loop in K2
		//Server stuff
		MessageSocketSendPacket = 0x44B370,// CMessageSocket::SendPacket(CPacket &)	0044B370	2751
		ClientConnectionSendPacket = 0x34EEC0,// CClientConnection::SendPacket(CPacket &)	0034EEC0	2750
		ClientConnectionSendReliablePacket = 0x350630,// CClientConnection::SendReliablePacket(CPacket &)	00350630	2753
		ClientConnectionSendGameData = 0x00350560,// CClientConnection::SendGameData(IBuffer const &,bool)	00350560	2747

		HostClientSendGameData = 0x35F860,// CHostClient::SendGameData(IBuffer const &,bool)	0035F860	2748
		//Handle NETCMD_SERVER_GAME_DATA = 0x0015D4A0,// cgame dll init 42
		ReceivePacket = 0x00364E50, // 
		GameEventSpawn = 0x002C0990, // CGameEvent::Spawn(void)	002C0990	958
		GameEntityAllocation = 0x001A89E0, // In Game EntitySync "bad snapshot"
		CEntityRegistryAllocate = 0x002ECFD0,//CEntityRegistry::Allocate(ushort,uint)	002ECFD0	74
		DeleteEntity = 0x001A8310, //Tried to delete entity #



	};

	enum class Packets
	{
		OrderMoveVM = 0x23FB18, // hero_holdAfterMove near IBuffer constructor
		SelectUnitsVM = 0x23FD68 //
	}; 
};