#pragma once
#include "Macros.h"
#include "Vector2f.h"
#include "Vector3f.h"
#include "Offsets.h"
#include "Core.h"
#include "GameInternal.h"

namespace HonSDK
{
	namespace Native
	{
		class Camera;

		class SceneManager
		{
		public:
			static SceneManager* GetInstance()
			{
				return *reinterpret_cast<SceneManager**>((DWORD)Core::hK2 + static_cast<__int32>(Offsets::Global::SceneManager));
			}
			MAKE_GET_PTR(Camera, CameraInstance, 0x4);
		};

		typedef bool(__fastcall*W2S)(Camera*, int edx, Vector3f*, Vector2f*);

		class DLLEXPORT Camera
		{
		protected:
		public:
			static Camera* GetInstance()
			{
				return GameInternal::GetInstance()->GetGameCamera();
			}

			bool WorldToScreen(Vector3f* position, Vector2f* pointOut);
		};
	}
}
