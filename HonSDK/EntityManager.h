#pragma once
#include "Macros.h"
#include <datetimeapi.h>
#include "ClientEntity.h"

namespace HonSDK
{
	namespace Native
	{
		template<typename T>
		struct BTreeNode
		{
			BTreeNode<T>* RightNode;
			BTreeNode<T>* ParentNode;
			BTreeNode<T>* LeftNode;
			int IndexValue;
			T Value;
			bool Unknown14;
			bool IsLastElement;
		};

		template<typename T>
		struct BTree
		{
			BTreeNode<T>* RootNode;
			int Size;



		};

		class EntityManager
		{
		public:
			MAKE_GET_PTR(BTree<int>, ClientEntityTree, 0x20);

		};
	}
}
