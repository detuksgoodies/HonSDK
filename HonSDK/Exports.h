#pragma once
#include "Core.h"


namespace HonSDK
{
	namespace Native
	{
		class DLLEXPORT Exports
		{
		public:
			FARPROC WorldToSceen;
			FARPROC CanSee;
			//EntityAbility
			FARPROC IsReady;
			FARPROC GetActualRemainingCooldowntime;
			FARPROC GetRemainingCooldowntime;
			FARPROC GetCooldownEndTime;
			FARPROC GetDynamicRange;
			//UnitEntity
			FARPROC IsEnemy;
			FARPROC GetActivePath;
			//CGameMechanics
			FARPROC GetArmorDamageAdjustment;


			static Exports* Exports::GetInstance()
			{
				static auto inst = new Exports();
				return inst;
			}

			Exports()
			{
				WorldToSceen = GetProcAddress(HonSDK::Native::Core::hK2, "?WorldToScreen@CCamera@@QBE_NABV?$CVec3@M@@AAV?$CVec2@M@@@Z");
				CanSee = GetProcAddress(HonSDK::Native::Core::hGameShared, "?CanSee@CPlayer@@QBE_NPBVIVisualEntity@@@Z");
				IsReady = GetProcAddress(HonSDK::Native::Core::hGameShared, "?IsReady@IEntityTool@@QBE_NXZ");
				GetActualRemainingCooldowntime = GetProcAddress(HonSDK::Native::Core::hGameShared, "?GetActualRemainingCooldownTime@IEntityTool@@QBEIXZ");
				GetRemainingCooldowntime = GetProcAddress(HonSDK::Native::Core::hGameShared, "?GetRemainingCooldownTime@IEntityTool@@QBEI_N@Z");
				GetCooldownEndTime = GetProcAddress(HonSDK::Native::Core::hGameShared, "?GetCooldownEndTime@IEntityTool@@QBEIXZ");
				GetDynamicRange = GetProcAddress(HonSDK::Native::Core::hGameShared, "?GetDynamicRange@IEntityTool@@QAEM_N@Z");
				IsEnemy = GetProcAddress(HonSDK::Native::Core::hGameShared, "?IsEnemy@IUnitEntity@@QBE_NPAV1@@Z");
				GetActivePath = GetProcAddress(HonSDK::Native::Core::hGameShared, "?GetActivePath@IUnitEntity@@QAEIXZ");
				GetArmorDamageAdjustment = GetProcAddress(HonSDK::Native::Core::hGameShared, "?GetArmorDamageAdjustment@CGameMechanics@@QBEMIM@Z");

			}

		};
	}
}
