#pragma once
#include <windows.h>

namespace HonSDK
{
	namespace Native
	{
		class Memory
		{
		public:

			static VOID WriteMemory(PVOID dwAdd, VOID *val, INT bytes);
			static VOID WriteFloat(DWORD dwAdd, FLOAT Value);
			static VOID WriteInteger(DWORD dwAdd, INT Value);
			static CHAR* ReadText(DWORD dwAdd);

			static DWORD FindPattern(DWORD dwdwAdd, DWORD dwLen, BYTE *bMask, char * szMask);
			static HINSTANCE lGetModuleHandle(LPCWSTR szModule);
		private:
			static BOOL bCompare(const BYTE* pData, const BYTE* bMask, const char* szMask);
		};
	}
}