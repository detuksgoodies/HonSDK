#include "stdafx.h"
#include "Game.h"
#include "EventHandler.h"
#include "GameInternal.h"
#include "Camera.h"
#include <winsock.h>
#include "Buffer.h"
#include "Packets.h"
#include "Action.h"
#include "ObjectManager.h"
#include "Vector3f.h"
#include "Vector2f.h"
#include "Packet.h"
#include "HeroEntity.h"
#include "GameEvent.h"
#include <set>
#include "CreepEntity.h"

namespace HonSDK
{
	namespace Native
	{
		std::unique_ptr<std::vector<GameEntity*>> Game::SpawnedEntities = std::make_unique<std::vector<GameEntity*>>();
		int Game::LastNetworkId = 0;
		bool SendMove = false;

		WNDPROC m_wndProc;
		LRESULT WINAPI hkWndProc(HWND, UINT, WPARAM, LPARAM);

		MAKE_HOOK<convention_type::cdecl_t, bool, int> Game_ClientSync;
		MAKE_HOOK<convention_type::stdcall_t, void, int, int> Game_MainLoop;
		MAKE_HOOK<convention_type::fastcall_t, int, int*, int, Packet*> Game_MessageSocketSendPacket;
		MAKE_HOOK<convention_type::fastcall_t, int, int*, int, Packet*> Game_ClientConnectionSendPacket;
		MAKE_HOOK<convention_type::fastcall_t, int, int*, int, Packet*> Game_ClientConnectionSendReliablePacket;

		MAKE_HOOK<convention_type::fastcall_t, void, HostClient*,int, Buffer*, bool> Game_HostClientSendGameData;
		MAKE_HOOK<convention_type::fastcall_t, void, GameInternal*, int, Packet*> Game_ReceivePacket;
		MAKE_HOOK<convention_type::fastcall_t, void, GameEvent*, int> Game_GameEventSpawn;
		MAKE_HOOK<convention_type::fastcall_t, GameEntity*, int, int, int, int> Game_GamEntityAllocated;
		MAKE_HOOK<convention_type::stdcall_t, int, int, int> Game_GamEntityDeAllocated;


		Game::Game()
		{
		}

		Game * HonSDK::Native::Game::GetInstance()
		{
			static auto inst = new Game();
			return inst;
		}

		bool Game::ApplyHooks() const
		{
			auto hWindow = FindWindowA(nullptr, "Heroes of Newerth");
			m_wndProc = reinterpret_cast<WNDPROC>(SetWindowLongPtr(hWindow, GWL_WNDPROC, static_cast<LONG>(reinterpret_cast<LONG_PTR>(hkWndProc))));

			Game_ClientSync.Apply((DWORD)Core::hCGame + (DWORD)Offsets::Game::OnGameSync, [](int dataSmth) -> bool
			{
				__asm pushad;
				EventHandler<2, OnClientSync>::GetInstance()->Trigger();
				__asm popad;
				return Game_ClientSync.CallOriginal(dataSmth);
			});

			Game_MainLoop.Apply((DWORD)Core::hK2 + (DWORD)Offsets::Game::OnGameUpdate, [](int ExitCode, int unknown) -> void
			{
				Game_MainLoop.CallOriginal(ExitCode, unknown);
				__asm pushad;

				for (GameEntity* entity : *SpawnedEntities)
				{
					int netId = *entity->GetNetworkId();
					if (netId <= LastNetworkId && !(netId<11000 && LastNetworkId>12000))
						continue;
					LastNetworkId = netId;
					EventHandler<8, OnGameEntityCreate, GameEntity*>::GetInstance()->Trigger(entity);
				}
				SpawnedEntities->clear();

				EventHandler<4, OnMainLoop>::GetInstance()->Trigger();
				if(SendMove)
				{
					//auto cursor = *GameInternal::GetInstance()->GetGameCursorContainer()->GetCursorPos();
					//Action::Move(cursor.GetX(), cursor.GetY());


					//Vector2f poin tOut;
					//Vector3f pointIn(1000, 1000, 0);
					//bool res = Camera::GetInstance()->WorldToScreen(&pointIn, &pointOut);
					//
					//Console::PrintLn("W2S: rex: %d X: %f, Y: %f", res, pointOut.GetX(), pointOut.GetY());
					//

					auto myhero = ObjectManager::GetMyHero();

					std::vector<GameEntity*> entities;
					ObjectManager::GetEntities(&entities);
					Console::PrintLn("Entities count: %d", entities.size());
					for (GameEntity* entity : entities)
					{
						if (!entity->IsHero())
							continue; 
						auto hero = (HeroEntity*)entity;
						auto armor = GameInternal::GetInstance()->GetMechanics()->GetArmorDamageAdjustment(hero->GetMagicArmorPercentage(), hero->GetMagicArmor());

						Console::PrintLn("Hero: %#010x", hero);
						Console::PrintLn("AttackMin: %f", hero->GetAttackDamangeBaseMin());
						Console::PrintLn("AttackMin: %f", hero->GetAttackDamangeBaseMin1());
						Console::PrintLn("AttackMin: %f", hero->GetAttackDamangeBaseMin2());
						Console::PrintLn("GetAttackDamangeAdded: %f", hero->GetAttackDamangeAdded());
						Console::PrintLn("Armor adjusted: %f", armor);

						//if(*entity->GetNetworkId() != heroId)
						//	continue;
						//Console::PrintLn("Entity %f %f %f %d", *hero->GetX(), *hero->GetY(), *hero->GetZ(), *hero->GetHealth());
						//continue;
						//for(int i=0;i<4;i++)
						//{
						//	auto ability = hero->GetAbility(i);
						//	Console::PrintLn("Ability: %#010x", ability);
						//	//continue;
						//	if(ability != nullptr)
						//	{
						//		Console::PrintLn("Ability slot: %d GetDynamicRange: %f", i, ability->GetManaCost());
						//	}
						//}
					}
					//Action::SelectEntities((GameEntity**)&heroes[0],heroes.size(), 1);
					//SendMove = false;
				}
				__asm popad;
			});
			
			Game_HostClientSendGameData.Apply((DWORD)Core::hK2 + (DWORD)Offsets::Game::HostClientSendGameData, [](HostClient* sender, int edx, Buffer* buffer, bool relaible) -> void
			{
				__asm pushad;
				auto process = EventHandler<5, OnSendGameData, Buffer*, bool>::GetInstance()->TriggerProcess(buffer, relaible);

				__asm popad;

				if (process)
					Game_HostClientSendGameData.CallOriginal(sender, edx, buffer, relaible);
				//return 0;
			});

			Game_ReceivePacket.Apply((DWORD)Core::hK2 + (DWORD)Offsets::Game::ReceivePacket, [](GameInternal* sender, int edx, Packet* buffer) -> void
			{
				__asm pushad;
				auto process = EventHandler<6, OnReceivePacket, Packet*, int>::GetInstance()->TriggerProcess(buffer, 0);
			
				__asm popad;
			
				if (process)
					Game_ReceivePacket.CallOriginal(sender, edx, buffer);
				//return 0;
			});

			Game_GameEventSpawn.Apply((DWORD)Core::hGameShared + (DWORD)Offsets::Game::GameEventSpawn, [](GameEvent* event, int edx) -> void
			{
				__asm pushad;
				auto process = EventHandler<7, OnGameEventSpawn, GameEvent*>::GetInstance()->TriggerProcess(event);
				__asm popad;

				if (process)
					Game_GameEventSpawn.CallOriginal(event, edx);
			});
			
			Game_GamEntityAllocated.Apply((DWORD)Core::hGameShared + (DWORD)Offsets::Game::CEntityRegistryAllocate, [](int entityRegistry, int edx, int unk2, int unk3) -> GameEntity*
			{
				GameEntity* entity = Game_GamEntityAllocated.CallOriginal(entityRegistry, edx, unk2, unk3);
				__asm pushad;
				SpawnedEntities->push_back(entity);
				__asm popad;
				return entity;
			});

			Game_GamEntityDeAllocated.Apply((DWORD)Core::hCGame + (DWORD)Offsets::Game::DeleteEntity, [](int entityTree, int index) -> int
			{
				__asm pushad;
				EventHandler<9, OnGameEntityDelete, int>::GetInstance()->Trigger(index);
				__asm popad;
				return Game_GamEntityDeAllocated.CallOriginal(entityTree, index);
			});


			return true;
			return Game_ClientSync.IsApplied() &&
				Game_MainLoop.IsApplied() &&
				Game_MessageSocketSendPacket.IsApplied() &&
				Game_ClientConnectionSendPacket.IsApplied();
		}

		bool Game::SendGameData(Buffer * buffer, bool reliable)
		{
			Game_HostClientSendGameData.CallDetour(GameInternal::GetInstance()->GetGameHostClient(), 0, buffer, reliable);
			return true;
		}

		void __fastcall DetourClientConnectionSendPacket(SOCKET* pThis, int EDX, Packet* packet)
		{
			
		}

		LRESULT WINAPI hkWndProc(HWND hwnd, UINT msg, WPARAM WParam, LPARAM LParam)
		{
			auto process = EventHandler<1, OnWndProc, HWND, UINT, WPARAM, LPARAM>::GetInstance()->TriggerProcess(hwnd, msg, WParam, LParam);
			LRESULT returnValue;

			if(msg == WM_KEYDOWN)
			{

				if (WParam == VK_SPACE)
				{
					//SendMove = true;
				}
			}

			if (msg == WM_KEYUP)
			{
				if (WParam == VK_SPACE)
				{
					//Bootstrapper::GetInstance()->Trigger(BootstrapEventType::Load);
					//auto cursor = *GameInternal::GetInstance()->GetGameCursorContainer()->GetCursorPos();
					//Console::PrintLn("Cursor X: %f", cursor.GetX());
					SendMove = false;
				}

				if (false && WParam == VK_F1)
				{
					Core::get_DetourInstance()->RemoveHooks();
					auto hWindow = FindWindowA(nullptr, "Heroes of Newerth");
					reinterpret_cast<WNDPROC>(SetWindowLongPtr(hWindow, GWL_WNDPROC, static_cast<LONG>(0)));
					//auto lua = new Lua();
				}
			}

			if (process)
			{
				return CallWindowProc(m_wndProc, hwnd, msg, WParam, LParam);
			}

			return 1;
		}
	}
}

