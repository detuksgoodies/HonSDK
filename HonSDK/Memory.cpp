#include "Memory.h"

namespace HonSDK
{
	namespace Native
	{
		VOID Memory::WriteMemory(PVOID dwAdd, void *val, int bytes)
		{

			DWORD d, ds;
			VirtualProtect(dwAdd, bytes, PAGE_EXECUTE_READWRITE, &d);
			memcpy(dwAdd, val, bytes);
			VirtualProtect(dwAdd, bytes, d, &ds);
		}

		VOID Memory::WriteFloat(DWORD dwAdd, float Value)
		{
			*(float*)dwAdd = Value;
		}

		VOID Memory::WriteInteger(DWORD dwAdd, int Value)
		{
			*(int*)dwAdd = Value;
		}

		CHAR* Memory::ReadText(DWORD dwAdd)
		{
			CHAR* Text = (CHAR*)dwAdd; //reversal of WriteText...
			return Text;
		}

		BOOL Memory::bCompare(const BYTE* pData, const BYTE* bMask, const char* szMask)
		{
			for (; *szMask; ++szMask, ++pData, ++bMask)
				if (*szMask == 'x' && *pData != *bMask)   return 0;
			return (*szMask) == NULL;
		}

		DWORD Memory::FindPattern(DWORD dwdwAdd, DWORD dwLen, BYTE *bMask, char * szMask)
		{
			for (DWORD i = 0; i<dwLen; i++)
				if (bCompare((BYTE*)(dwdwAdd + i), bMask, szMask))  return (DWORD)(dwdwAdd + i);
			return 0;
		}

		HINSTANCE Memory::lGetModuleHandle(LPCWSTR szModule)
		{
			HINSTANCE hModule = NULL;
			if (!(hModule = GetModuleHandle(szModule)))
			{
				hModule = LoadLibrary(szModule);
			}
			return hModule;
		}
	}
}
