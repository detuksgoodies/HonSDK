#pragma once

namespace HonSDK
{
	namespace Native
	{
		struct Packet;

		class ObjectManagerTests
		{
		public:
			static void TestEntitiesCount();
			static void TestEntitiesOutput();
			static void TestEntitiesTree();
		};
	}
}