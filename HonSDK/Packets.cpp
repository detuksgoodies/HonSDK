#include "Packets.h"
#include "Core.h"
#include "Offsets.h"

//TODO: change to make it thread safe I guess
char Data[0x800];

HonSDK::Native::ByteBuffer HonSDK::Native::PacketBuilder::DateBuffer;

void HonSDK::Native::PacketBuilder::CreateMovePacket(Buffer* buffer, float x, float y, char flag)
{
	buffer->VirtualTable = (DWORD)Core::hCGame + (DWORD)Offsets::Packets::OrderMoveVM;
	buffer->Size = 0x12;
	buffer->AllocatedSize = 0x12;
	buffer->CurrentOffset = 0;
	buffer->SomeFlag = 0;
	DateBuffer.clearFast();

	DateBuffer.putChar(0x1e);//header
	DateBuffer.putChar(flag);//flag 0x2

	DateBuffer.putFloat(x);
	DateBuffer.putFloat(y);
	//Wierd ending
	DateBuffer.putBytes("\x00\x00\x00\x00\x00\xFF\xFF\x00", 8);

	buffer->Data = reinterpret_cast<unsigned char*> (&DateBuffer.buf[0]);;

}

void HonSDK::Native::PacketBuilder::CreateHoldPacket(Buffer* buffer)
{
	buffer->VirtualTable = (DWORD)Core::hCGame + (DWORD)Offsets::Packets::OrderMoveVM;
	buffer->Size = 0x5;
	buffer->AllocatedSize = 0x5;
	buffer->CurrentOffset = 0;
	buffer->SomeFlag = 0;
	DateBuffer.clearFast();

	DateBuffer.putChar(0x21);//header
	//Wierd ending
	DateBuffer.putBytes("\x00\xFF\xFF\x00", 4);

	buffer->Data = reinterpret_cast<unsigned char*> (&DateBuffer.buf[0]);;

}

void HonSDK::Native::PacketBuilder::CreateSelectUnitsPacket(Buffer * buffer, GameEntity** entieies, int count, char flag)
{
	buffer->VirtualTable = (DWORD)Core::hCGame + (DWORD)Offsets::Packets::SelectUnitsVM;
	buffer->Size = 0x4+ count *2;
	buffer->AllocatedSize = buffer->Size+6;
	buffer->CurrentOffset = 0;
	buffer->SomeFlag = 0;

	DateBuffer.clearFast();

	DateBuffer.putChar(0x5a);//header
	DateBuffer.putChar(flag);//flag 0x1
	for (int i =0; i < count; i ++)
	{
		DateBuffer.putShort(*entieies[i]->GetNetworkId());
	}

	// Ending
	DateBuffer.putBytes("\xFF\xFF", 2);

	buffer->Data = reinterpret_cast<unsigned char*> (&DateBuffer.buf[0]);;
}

void HonSDK::Native::PacketBuilder::CreateAttackPacket(Buffer* buffer, GameEntity* entity, char flag)
{
	buffer->VirtualTable = (DWORD)Core::hCGame + (DWORD)Offsets::Packets::OrderMoveVM;
	buffer->Size = 12;
	buffer->AllocatedSize = 12;
	buffer->CurrentOffset = 0;
	buffer->SomeFlag = 0;

	DateBuffer.clearFast();

	DateBuffer.putChar(0x1f);//header
	DateBuffer.putChar(flag);//flag 0x9

	DateBuffer.putShort(*entity->GetNetworkId());

	// Ending
	DateBuffer.putBytes("\x00\x00\x00\x00\x00\xFF\xFF\x00", 8);

	buffer->Data = reinterpret_cast<unsigned char*> (&DateBuffer.buf[0]);
}

void HonSDK::Native::PacketBuilder::CreateCastSpellPacket(Buffer * buffer, GameEntity * entity, char slot, float x, float y)
{
	buffer->VirtualTable = (DWORD)Core::hCGame + (DWORD)Offsets::Packets::OrderMoveVM;
	buffer->Size = 14;
	buffer->AllocatedSize = 14;
	buffer->CurrentOffset = 0;
	buffer->SomeFlag = 0;
	DateBuffer.clearFast();

	DateBuffer.putChar(0x1c);//header
	DateBuffer.putShort(*entity->GetNetworkId());//header
	DateBuffer.putChar(slot);//flag 0x2
	DateBuffer.putBytes("\x00\x00", 2);// dunno
	DateBuffer.putFloat(x);
	DateBuffer.putFloat(y);

	buffer->Data = reinterpret_cast<unsigned char*> (&DateBuffer.buf[0]);;
}

void HonSDK::Native::PacketBuilder::CreateCastSpellPacket(Buffer * buffer, GameEntity * entity, char slot)
{
	buffer->VirtualTable = (DWORD)Core::hCGame + (DWORD)Offsets::Packets::OrderMoveVM;
	buffer->Size = 14;
	buffer->AllocatedSize = 14;
	buffer->CurrentOffset = 0;
	buffer->SomeFlag = 0;
	DateBuffer.clearFast();

	DateBuffer.putChar(0x1b);//header
	DateBuffer.putShort(*entity->GetNetworkId());//header
	DateBuffer.putChar(slot);//flag 0x2
	DateBuffer.putBytes("\x00\x00", 2);// dunno

	buffer->Data = reinterpret_cast<unsigned char*> (&DateBuffer.buf[0]);;
}

void HonSDK::Native::PacketBuilder::CreateCastSpellPacket(Buffer * buffer, GameEntity * entity, char slot, GameEntity * target)
{
	buffer->VirtualTable = (DWORD)Core::hCGame + (DWORD)Offsets::Packets::OrderMoveVM;
	buffer->Size = 8;
	buffer->AllocatedSize = 8;
	buffer->CurrentOffset = 0;
	buffer->SomeFlag = 0;
	DateBuffer.clearFast();

	DateBuffer.putChar(0x1d);//header
	DateBuffer.putShort(*entity->GetNetworkId());//header
	DateBuffer.putChar(slot);//flag 0x2
	DateBuffer.putBytes("\x00\x00", 2);// dunno
	DateBuffer.putShort(*target->GetNetworkId());//header

	buffer->Data = reinterpret_cast<unsigned char*> (&DateBuffer.buf[0]);;
}
