#pragma once
#include "Exports.h"
#include "UnitEntity.h"

namespace HonSDK
{
	namespace Native
	{
		class Player
		{
		public:
			//MAKE_GET(int, NetworkId, 0x10);
			MAKE_GET(int, HeroEntityNetId, 0x80);
			//Something about Scrolling Cplayer:StartScroll
			//MAKE_GET(int, MousePosX, 0x9C);
			//MAKE_GET(int, MousePosY, 0x9D);
			//IMPLVTFUNC(bool, IsHero, 0xB4)

			bool CanSee(UnitEntity* entity)
			{
				return ((bool(__fastcall*)(Player*, int, UnitEntity*))Exports::GetInstance()->CanSee)(this, 0, entity);
			}
		};
	}
}
