#pragma once
#include <d3d9.h>
#include <d3dx9.h>

#pragma comment(lib,"d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

#include "Memory.h"
#include "Console.h"
#include "Detour.hpp"
#include "EventHandler.h"
#include "Core.h"

namespace HonSDK
{
	namespace Native
	{
		class DLLEXPORT DirectX9
		{
		public:
			static DirectX9* GetInstance();
			bool ApplyHooks() const;
			LPDIRECT3DDEVICE9 GetDevice();
		private:
		};
	}
}