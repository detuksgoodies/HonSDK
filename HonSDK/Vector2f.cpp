#include "Vector2f.h"
namespace HonSDK
{
	namespace Native
	{
		Vector2f::Vector2f()
		{

		}

		Vector2f::Vector2f(float xx, float yy) : X(xx), Y(yy) {}

		Vector2f Vector2f::SwitchYZ() const
		{
			return Vector2f(X, Y);
		}

		bool Vector2f::IsValid() const
		{
			return X != 0 && Y != 0;
		}

		Vector2f::operator float*()
		{
			return &X;
		}

		float Vector2f::GetX() const
		{
			return X;
		}

		float Vector2f::GetY() const
		{
			return Y;
		}

		float Vector2f::DistanceTo(const Vector2f& v) const
		{
			return sqrt(pow(v.X - X, 2) + pow(v.Y - Y, 2));
		}

		Vector2f Vector2f::Randomize()
		{
			auto xRand = X + (rand() % -50 + 100);
			auto yRand = Y + (rand() % -30 + 70);

			return Vector2f(xRand, yRand);
		}

	}
}
