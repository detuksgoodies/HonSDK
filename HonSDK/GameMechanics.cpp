#include "GameMechanics.h"

double HonSDK::Native::GameMechanics::GetArmorDamageAdjustment(int armorPercentage, float armor)
{
	return ((double(__fastcall*)(GameMechanics*, int, int, float))Exports::GetInstance()->GetArmorDamageAdjustment)(this, 0, armorPercentage, armor);
}
