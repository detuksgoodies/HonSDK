﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HonSDK.Sandbox
{
    internal static class Input
    {
        public static void Subscribe()
        {
            AppDomain.CurrentDomain.AssemblyResolve += SandboxDomain.DomainOnAssemblyResolve;
            Game.OnWndProc += Game_OnWndProc;
        }
        
        internal static void Game_OnWndProc(WndEventArgs args)
        {
            if (args.Msg == 0x0101 /*WM_KEYUP*/)
            {
                if (args.WParam == 0x74)
                {
                    Sandbox.Reload();
                }

                if (args.WParam == 0x75)
                {
                    Sandbox.Unload();
                }
            }
        }
    }
}
