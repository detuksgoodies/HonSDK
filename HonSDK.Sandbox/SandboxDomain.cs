﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace HonSDK.Sandbox
{
    internal class SandboxDomain : MarshalByRefObject
    {
        internal static readonly Dictionary<string, Assembly> LoadedLibraries = new Dictionary<string, Assembly>();
        internal static readonly List<string> LoadedPlugins = new List<string>();
        internal static SandboxDomain Instance { get; set; }
        internal AppDomain DomainHandle { get; private set; }
        static SandboxDomain()
        {
            // Listen to requried events
            AppDomain.CurrentDomain.AssemblyResolve += DomainOnAssemblyResolve;
        }
        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }
        internal static SandboxDomain CreateDomain(string domainName)
        {
            SandboxDomain domain = null;

            try
            {
                if (string.IsNullOrEmpty(domainName))
                {
                    domainName = "Sandbox" + Guid.NewGuid().ToString("N") + "Domain";
                }

                var appDomain = AppDomain.CreateDomain(domainName);

                // Create a new Domain instance
                domain = (SandboxDomain)Activator.CreateInstanceFrom(appDomain, Assembly.GetExecutingAssembly().Location, typeof(SandboxDomain).FullName).Unwrap();
                if (domain != null)
                {
                    domain.DomainHandle = appDomain;
                    domain.Initialize();
                }
            }
            catch (Exception e)
            {
                Logs.Log("Sandbox: An exception occurred creating the AppDomain!");
                Logs.Log(e.ToString());
            }

            return domain;
        }

        private void Initialize()
        {
            // Listen to unhandled exceptions
            DomainHandle.UnhandledException += OnUnhandledException;
        }

        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs unhandledExceptionEventArgs)
        {
            Logs.Log("Sandbox: Unhandled addon exception:");
            var securityException = unhandledExceptionEventArgs.ExceptionObject as SecurityException;
            if (securityException != null)
            {
                Logs.Log(unhandledExceptionEventArgs.ExceptionObject.ToString());
            }

            Logs.PrintException(unhandledExceptionEventArgs.ExceptionObject);
        }

        internal string CopyToTemp(string file)
        {
            var tempDir = Path.Combine(Path.GetDirectoryName(file),"temp");
            var tempFile = Path.Combine(tempDir, Path.GetFileName(file));
            try
            {
                File.Delete(tempFile);
            }
            catch (Exception)
            {
            }
            File.Copy(file, tempFile);
            return tempFile;
        }

        internal void LoadPlugin(string path, string[] args)
        {
            if (File.Exists(path))
            {
                // Get the AssemblyName of the addon by the path
                AssemblyName assemblyName = AssemblyName.GetAssemblyName(CopyToTemp(path));

                // Try to execute the addon
                DomainHandle.ExecuteAssemblyByName(assemblyName, args);
                if (!LoadedPlugins.Contains(assemblyName.Name))
                {
                    LoadedPlugins.Add(assemblyName.Name);
                }
            }
        }

        internal static bool IsSystemAssembly(string path)
        {
            return path.EndsWith(".dll") || Path.GetDirectoryName(path).EndsWith("Libraries");
        }

        internal static void UnloadDomain(SandboxDomain domain)
        {
            AppDomain.Unload(domain.DomainHandle);
        }

        internal static Assembly DomainOnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            Assembly resolvedAssembly = null;
            try
            {

                if (args.Name.Contains(".resources") || args.Name.Equals("HonSDK.Sandbox"))
                {
                    return null;
                }
                Console.WriteLine("Resolving: "+args.Name);
                var assemblyName = new AssemblyName(args.Name);
                var assemblyToken = assemblyName.GenerateToken();

                if (Assembly.GetExecutingAssembly().FullName.Equals(args.Name))
                {
                    // Executing assembly
                    resolvedAssembly = Assembly.GetExecutingAssembly();
                }
                else if(args.Name == "HonSDK.Wrapper")
                {
                    resolvedAssembly = Assembly.LoadFrom(Path.Combine(AssemblyDirectory, "HonSDK.Wrapper.dll"));
                }
                else
                {
                    string path = FindPlugin(assemblyName);
                    if(path == null)
                        path = FindLibrary(assemblyName);
                    if (path != null)
                    {
                        if (LoadedLibraries.ContainsKey(assemblyToken))
                        {
                            resolvedAssembly = LoadedLibraries[assemblyToken];
                        }
                        else
                        {
                            resolvedAssembly = Assembly.LoadFrom(path);

                            LoadedLibraries.Add(assemblyToken, resolvedAssembly);
                        }
                    }
                    else
                    {
                        Logs.Log("Failed to find in dir: " + AssemblyDirectory);
                        Logs.Log("Failed to find assembly: " + args.Name);
                    }
                }

            }
            catch (Exception e)
            {
                Logs.Log("Sandbox: Failed to resolve addon:");
                Logs.Log(e.ToString());
            }
            return resolvedAssembly;
        }
        internal static string FindLibrary(AssemblyName assemblyName)
        {
            foreach (var file in new[] {Assembly.GetExecutingAssembly().Location}
                .Where(dir => Directory.Exists(dir)).SelectMany(Directory.EnumerateFiles))
            {
                if (AssemblyName.GetAssemblyName(file).Name.Equals(assemblyName.Name))
                {
                    return file;
                }
            }

            return null;
        }

        internal static string FindPlugin(AssemblyName assemblyName)
        {
            foreach (var file in new[] { Path.Combine(Assembly.GetExecutingAssembly().Location, "Plugins") }
                .Where(dir => Directory.Exists(dir)).SelectMany(Directory.EnumerateFiles))
            {
                if (AssemblyName.GetAssemblyName(file).Name.Equals(assemblyName.Name))
                {
                    return file;
                }
            }

            return null;
        }

    }
}
