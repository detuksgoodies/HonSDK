﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HonSDK.Sandbox
{
    internal static class Extensions
    {
        internal static string GenerateToken(this AssemblyName assemblyName)
        {
            return assemblyName.Name + assemblyName.GetPublicKeyToken().Select(o => o.ToString("x2")).Concat(new[] { string.Empty }).Aggregate(string.Concat);
        }
    }
}
