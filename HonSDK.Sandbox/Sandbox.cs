﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HonSDK.Sandbox
{
    public class Sandbox
    {
        public static int Bootstrap(string ignore)
        {
            try
            {
                Logs.Log("Starting sandbox!");
                Reload();
                Input.Subscribe();
            }
            catch (Exception ex)
            {
                Logs.PrintException(ex);
            }

            return 0;
        }

        static Sandbox()
        {
            AppDomain.CurrentDomain.ProcessExit += DomainOnProcessExit;
            AppDomain.CurrentDomain.AssemblyResolve += SandboxDomain.DomainOnAssemblyResolve;
            AppDomain.CurrentDomain.UnhandledException += delegate (object sender, UnhandledExceptionEventArgs args)
            {
                Logs.Log("Sandbox: Unhandled Sandbox exception");
                Logs.PrintException(args.ExceptionObject);
            };
        }

        private static void DomainOnProcessExit(object sender, EventArgs e)
        {
            Logs.Log("Sandbox: Shutting down...");
        }

        internal static void Load()
        {
            // Set thread culture
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;
            
            if (SandboxDomain.Instance == null)
            {
                return;
            }

            try
            {
                var plugsDir = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Plugins");
                var plugins = new[] { plugsDir }
                    .Where(dir => Directory.Exists(dir)).SelectMany(Directory.EnumerateFiles)
                    .Where( file => file.EndsWith(".exe"));

                Logs.Log("Loading Plugins");
                foreach (var plugin in plugins)
                {
                    Logs.Log("Load "+ plugin);
                    SandboxDomain.Instance.LoadPlugin(plugin, new string[1]);
                }
            }
            catch (Exception e)
            {
                Logs.Log("Sandbox: Loading assemblies failed");
                Logs.Log(e.ToString());
                return;
            }
        }

        internal static void Reload()
        {
            Unload();
            CreateApplicationDomain();
            Load();
        }

        internal static void Unload()
        {
            if (SandboxDomain.Instance == null)
                return;
            try
            {
                SandboxDomain.UnloadDomain(SandboxDomain.Instance);
            }
            catch (Exception e)
            {
                Logs.Log("Sandbox: Unloading AppDomain failed");
                Logs.Log(e.ToString());
            }
            SandboxDomain.Instance = null;
        }


        private static void CreateApplicationDomain()
        {
            if (SandboxDomain.Instance != null)
            {
                return;
            }

            try
            {
                SandboxDomain.Instance = SandboxDomain.CreateDomain("SandboxDomain");

                if (SandboxDomain.Instance == null)
                {
                    Logs.Log("Sandbox: AppDomain creation failed, please report this error!");
                }
            }
            catch (Exception e)
            {
                Logs.Log("Sandbox: Error during AppDomain creation");
                Logs.Log(e.ToString());
            }
        }

    }
}
